#include <hxcpp.h>

#ifndef INCLUDED_PlayerShip
#include <PlayerShip.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_input_FlxKeyManager
#include <flixel/input/FlxKeyManager.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInputManager
#include <flixel/input/IFlxInputManager.h>
#endif
#ifndef INCLUDED_flixel_input_keyboard_FlxKeyboard
#include <flixel/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_math_FlxPoint
#include <flixel/math/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxPooled
#include <flixel/util/IFlxPooled.h>
#endif

Void PlayerShip_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,hx::Null< Float >  __o_Width,hx::Null< Float >  __o_Height)
{
HX_STACK_FRAME("PlayerShip","new",0xea13034f,"PlayerShip.new","PlayerShip.hx",17,0xc1e91b01)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(__o_Width,"Width")
HX_STACK_ARG(__o_Height,"Height")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
Float Width = __o_Width.Default(0);
Float Height = __o_Height.Default(0);
{
	HX_STACK_LINE(20)
	int tmp = ::flixel::FlxG_obj::width;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(20)
	Float tmp1 = (Float(tmp) / Float((int)2));		HX_STACK_VAR(tmp1,"tmp1");
	HX_STACK_LINE(20)
	Float tmp2 = (tmp1 - (int)12);		HX_STACK_VAR(tmp2,"tmp2");
	HX_STACK_LINE(20)
	int tmp3 = ::flixel::FlxG_obj::height;		HX_STACK_VAR(tmp3,"tmp3");
	HX_STACK_LINE(20)
	int tmp4 = (tmp3 - (int)64);		HX_STACK_VAR(tmp4,"tmp4");
	HX_STACK_LINE(20)
	super::__construct(tmp2,tmp4,HX_HCSTRING("assets/images/Level2/SpaceshipSSS.png","\x22","\x71","\x8c","\x8a"));
}
;
	return null();
}

//PlayerShip_obj::~PlayerShip_obj() { }

Dynamic PlayerShip_obj::__CreateEmpty() { return  new PlayerShip_obj; }
hx::ObjectPtr< PlayerShip_obj > PlayerShip_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,hx::Null< Float >  __o_Width,hx::Null< Float >  __o_Height)
{  hx::ObjectPtr< PlayerShip_obj > _result_ = new PlayerShip_obj();
	_result_->__construct(__o_X,__o_Y,__o_Width,__o_Height);
	return _result_;}

Dynamic PlayerShip_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PlayerShip_obj > _result_ = new PlayerShip_obj();
	_result_->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3]);
	return _result_;}

Void PlayerShip_obj::update( Float elapsed){
{
		HX_STACK_FRAME("PlayerShip","update",0xa94b927a,"PlayerShip.update","PlayerShip.hx",26,0xc1e91b01)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(28)
		::flixel::math::FlxPoint tmp = this->velocity;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(28)
		tmp->set_x((int)0);
		HX_STACK_LINE(31)
		::flixel::input::keyboard::FlxKeyboard tmp1 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(31)
		bool tmp2 = tmp1->checkKeyArrayState(Array_obj< int >::__new().Add((int)37).Add((int)65),(int)1);		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(31)
		if ((tmp2)){
			HX_STACK_LINE(33)
			::flixel::math::FlxPoint tmp3 = this->velocity;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(33)
			::flixel::math::FlxPoint _g = tmp3;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(33)
			Float tmp4 = (_g->x - (int)150);		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(33)
			_g->set_x(tmp4);
		}
		HX_STACK_LINE(36)
		::flixel::input::keyboard::FlxKeyboard tmp3 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(36)
		bool tmp4 = tmp3->checkKeyArrayState(Array_obj< int >::__new().Add((int)39).Add((int)68),(int)1);		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(36)
		if ((tmp4)){
			HX_STACK_LINE(38)
			::flixel::math::FlxPoint tmp5 = this->velocity;		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(38)
			::flixel::math::FlxPoint _g = tmp5;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(38)
			Float tmp6 = (_g->x + (int)150);		HX_STACK_VAR(tmp6,"tmp6");
			HX_STACK_LINE(38)
			_g->set_x(tmp6);
		}
		HX_STACK_LINE(42)
		Float tmp5 = elapsed;		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(42)
		this->super::update(tmp5);
		HX_STACK_LINE(47)
		Float tmp6 = this->x;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(47)
		int tmp7 = ::flixel::FlxG_obj::width;		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(47)
		Float tmp8 = this->get_width();		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(47)
		Float tmp9 = (tmp7 - tmp8);		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(47)
		Float tmp10 = (tmp9 - (int)4);		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(47)
		bool tmp11 = (tmp6 > tmp10);		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(47)
		if ((tmp11)){
			HX_STACK_LINE(49)
			int tmp12 = ::flixel::FlxG_obj::width;		HX_STACK_VAR(tmp12,"tmp12");
			HX_STACK_LINE(49)
			Float tmp13 = this->get_width();		HX_STACK_VAR(tmp13,"tmp13");
			HX_STACK_LINE(49)
			Float tmp14 = (tmp12 - tmp13);		HX_STACK_VAR(tmp14,"tmp14");
			HX_STACK_LINE(49)
			Float tmp15 = (tmp14 - (int)4);		HX_STACK_VAR(tmp15,"tmp15");
			HX_STACK_LINE(49)
			this->set_x(tmp15);
		}
		HX_STACK_LINE(53)
		Float tmp12 = this->x;		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(53)
		bool tmp13 = (tmp12 < (int)4);		HX_STACK_VAR(tmp13,"tmp13");
		HX_STACK_LINE(53)
		if ((tmp13)){
			HX_STACK_LINE(55)
			this->set_x((int)4);
		}
		HX_STACK_LINE(57)
		Float tmp14 = elapsed;		HX_STACK_VAR(tmp14,"tmp14");
		HX_STACK_LINE(57)
		this->super::update(tmp14);
	}
return null();
}



PlayerShip_obj::PlayerShip_obj()
{
}

Dynamic PlayerShip_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PlayerShip_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PlayerShip_obj::__mClass,"__mClass");
};

#endif

hx::Class PlayerShip_obj::__mClass;

void PlayerShip_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("PlayerShip","\xdd","\xaf","\xc5","\xa5");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< PlayerShip_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

