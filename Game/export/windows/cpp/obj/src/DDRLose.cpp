#include <hxcpp.h>

#ifndef INCLUDED_DDRLose
#include <DDRLose.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_FlxInput
#include <flixel/input/FlxInput.h>
#endif
#ifndef INCLUDED_flixel_input_FlxPointer
#include <flixel/input/FlxPointer.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInput
#include <flixel/input/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInputManager
#include <flixel/input/IFlxInputManager.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void DDRLose_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("DDRLose","new",0x24370c39,"DDRLose.new","DDRLose.hx",6,0xa2071d57)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(6)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(6)
	super::__construct(tmp);
}
;
	return null();
}

//DDRLose_obj::~DDRLose_obj() { }

Dynamic DDRLose_obj::__CreateEmpty() { return  new DDRLose_obj; }
hx::ObjectPtr< DDRLose_obj > DDRLose_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< DDRLose_obj > _result_ = new DDRLose_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic DDRLose_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< DDRLose_obj > _result_ = new DDRLose_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void DDRLose_obj::create( ){
{
		HX_STACK_FRAME("DDRLose","create",0x371258c3,"DDRLose.create","DDRLose.hx",9,0xa2071d57)
		HX_STACK_THIS(this)
		HX_STACK_LINE(10)
		this->super::create();
		HX_STACK_LINE(11)
		::flixel::FlxSprite tmp = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/GameOVer.png","\xcc","\xfa","\x3c","\x8b"));		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(11)
		this->add(tmp);
		HX_STACK_LINE(12)
		::flixel::FlxSprite tmp1 = ::flixel::FlxSprite_obj::__new((int)5,(int)125,HX_HCSTRING("assets/images/Level4/message4.png","\xdc","\x85","\x63","\x54"));		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(12)
		this->add(tmp1);
	}
return null();
}


Void DDRLose_obj::update( Float elapsed){
{
		HX_STACK_FRAME("DDRLose","update",0x420877d0,"DDRLose.update","DDRLose.hx",16,0xa2071d57)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(17)
		Float tmp = elapsed;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(17)
		this->super::update(tmp);
		HX_STACK_LINE(18)
		bool tmp1;		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(18)
		{
			HX_STACK_LINE(18)
			::flixel::input::mouse::FlxMouse tmp2 = ::flixel::FlxG_obj::mouse;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(18)
			::flixel::input::mouse::FlxMouseButton _this = tmp2->_leftButton;		HX_STACK_VAR(_this,"_this");
			HX_STACK_LINE(18)
			bool tmp3 = (_this->current == (int)1);		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(18)
			bool tmp4 = !(tmp3);		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(18)
			if ((tmp4)){
				HX_STACK_LINE(18)
				tmp1 = (_this->current == (int)2);
			}
			else{
				HX_STACK_LINE(18)
				tmp1 = true;
			}
		}
		HX_STACK_LINE(18)
		if ((tmp1)){
			HX_STACK_LINE(20)
			::MenuState tmp2 = ::MenuState_obj::__new(null());		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(20)
			::flixel::FlxState nextState = tmp2;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(20)
			::flixel::FlxGame tmp3 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(20)
			::flixel::FlxState tmp4 = nextState;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(20)
			bool tmp5 = tmp3->_state->switchTo(tmp4);		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(20)
			if ((tmp5)){
				HX_STACK_LINE(20)
				::flixel::FlxGame tmp6 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp6,"tmp6");
				HX_STACK_LINE(20)
				tmp6->_requestedState = nextState;
			}
		}
	}
return null();
}



DDRLose_obj::DDRLose_obj()
{
}

Dynamic DDRLose_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(DDRLose_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(DDRLose_obj::__mClass,"__mClass");
};

#endif

hx::Class DDRLose_obj::__mClass;

void DDRLose_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("DDRLose","\xc7","\x7b","\x64","\xa5");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< DDRLose_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

