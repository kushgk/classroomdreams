#include <hxcpp.h>

#ifndef INCLUDED_Book
#include <Book.h>
#endif
#ifndef INCLUDED_BoxerLose
#include <BoxerLose.h>
#endif
#ifndef INCLUDED_PlayerBoxer
#include <PlayerBoxer.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_FlxKeyManager
#include <flixel/input/FlxKeyManager.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInputManager
#include <flixel/input/IFlxInputManager.h>
#endif
#ifndef INCLUDED_flixel_input_keyboard_FlxKeyboard
#include <flixel/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void PlayerBoxer_obj::__construct()
{
HX_STACK_FRAME("PlayerBoxer","new",0x7bb69469,"PlayerBoxer.new","PlayerBoxer.hx",17,0x1783ab27)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(18)
	super::__construct((int)130,(int)100,null());
	HX_STACK_LINE(20)
	this->loadGraphic(HX_HCSTRING("assets/images/Level1/MCSS.png","\xa8","\xa1","\xce","\x7a"),true,(int)64,(int)64,null(),null());
	HX_STACK_LINE(21)
	::flixel::animation::FlxAnimationController tmp = this->animation;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(21)
	tmp->add(HX_HCSTRING("RightPunch","\x52","\xa8","\x00","\xc4"),Array_obj< int >::__new().Add((int)6).Add((int)7).Add((int)8).Add((int)9),(int)8,false,null(),null());
	HX_STACK_LINE(22)
	::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
	HX_STACK_LINE(22)
	tmp1->add(HX_HCSTRING("LeftPunch","\x07","\x8c","\x3d","\x81"),Array_obj< int >::__new().Add((int)38).Add((int)39).Add((int)40).Add((int)41),(int)8,false,null(),null());
	HX_STACK_LINE(23)
	::flixel::animation::FlxAnimationController tmp2 = this->animation;		HX_STACK_VAR(tmp2,"tmp2");
	HX_STACK_LINE(23)
	tmp2->add(HX_HCSTRING("Hit","\x33","\xfe","\x36","\x00"),Array_obj< int >::__new().Add((int)12).Add((int)13),(int)5,false,null(),null());
	HX_STACK_LINE(24)
	::flixel::animation::FlxAnimationController tmp3 = this->animation;		HX_STACK_VAR(tmp3,"tmp3");
	HX_STACK_LINE(24)
	tmp3->add(HX_HCSTRING("Ded","\xa3","\xf1","\x33","\x00"),Array_obj< int >::__new().Add((int)18).Add((int)19).Add((int)20).Add((int)21).Add((int)22).Add((int)23),(int)5,false,null(),null());
	HX_STACK_LINE(25)
	::flixel::animation::FlxAnimationController tmp4 = this->animation;		HX_STACK_VAR(tmp4,"tmp4");
	HX_STACK_LINE(25)
	tmp4->add(HX_HCSTRING("Idle","\x34","\xd3","\x8c","\x30"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2),(int)5,true,null(),null());
	HX_STACK_LINE(26)
	::flixel::animation::FlxAnimationController tmp5 = this->animation;		HX_STACK_VAR(tmp5,"tmp5");
	HX_STACK_LINE(26)
	tmp5->add(HX_HCSTRING("LefDod","\xcc","\xc0","\x37","\xce"),Array_obj< int >::__new().Add((int)24).Add((int)25).Add((int)26).Add((int)27).Add((int)28),(int)8,false,null(),null());
	HX_STACK_LINE(27)
	::flixel::animation::FlxAnimationController tmp6 = this->animation;		HX_STACK_VAR(tmp6,"tmp6");
	HX_STACK_LINE(27)
	tmp6->add(HX_HCSTRING("RigDod","\xa9","\x73","\x9f","\x82"),Array_obj< int >::__new().Add((int)55).Add((int)56).Add((int)57).Add((int)58).Add((int)59),(int)8,false,null(),null());
	HX_STACK_LINE(28)
	::flixel::animation::FlxAnimationController tmp7 = this->animation;		HX_STACK_VAR(tmp7,"tmp7");
	HX_STACK_LINE(28)
	tmp7->play(HX_HCSTRING("Idle","\x34","\xd3","\x8c","\x30"),null(),null(),null());
	HX_STACK_LINE(30)
	Dynamic tmp8 = this->stateChange_dyn();		HX_STACK_VAR(tmp8,"tmp8");
	HX_STACK_LINE(30)
	::flixel::animation::FlxAnimationController tmp9 = this->animation;		HX_STACK_VAR(tmp9,"tmp9");
	HX_STACK_LINE(30)
	tmp9->finishCallback = tmp8;
	HX_STACK_LINE(32)
	this->health = (int)200;
}
;
	return null();
}

//PlayerBoxer_obj::~PlayerBoxer_obj() { }

Dynamic PlayerBoxer_obj::__CreateEmpty() { return  new PlayerBoxer_obj; }
hx::ObjectPtr< PlayerBoxer_obj > PlayerBoxer_obj::__new()
{  hx::ObjectPtr< PlayerBoxer_obj > _result_ = new PlayerBoxer_obj();
	_result_->__construct();
	return _result_;}

Dynamic PlayerBoxer_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PlayerBoxer_obj > _result_ = new PlayerBoxer_obj();
	_result_->__construct();
	return _result_;}

Void PlayerBoxer_obj::update( Float elapsed){
{
		HX_STACK_FRAME("PlayerBoxer","update",0x68a82da0,"PlayerBoxer.update","PlayerBoxer.hx",41,0x1783ab27)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(42)
		Float tmp = this->health;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(42)
		bool tmp1 = (tmp <= (int)0);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(42)
		if ((tmp1)){
			HX_STACK_LINE(43)
			::BoxerLose tmp2 = ::BoxerLose_obj::__new(null());		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(43)
			::flixel::FlxState nextState = tmp2;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(43)
			::flixel::FlxGame tmp3 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(43)
			::flixel::FlxState tmp4 = nextState;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(43)
			bool tmp5 = tmp3->_state->switchTo(tmp4);		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(43)
			if ((tmp5)){
				HX_STACK_LINE(43)
				::flixel::FlxGame tmp6 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp6,"tmp6");
				HX_STACK_LINE(43)
				tmp6->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(45)
		::flixel::input::keyboard::FlxKeyboard tmp2 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(45)
		bool tmp3 = tmp2->checkKeyArrayState(Array_obj< int >::__new().Add((int)88),(int)1);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(45)
		if ((tmp3)){
			HX_STACK_LINE(47)
			::flixel::animation::FlxAnimationController tmp4 = this->animation;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(47)
			tmp4->play(HX_HCSTRING("RightPunch","\x52","\xa8","\x00","\xc4"),null(),null(),null());
		}
		HX_STACK_LINE(50)
		::flixel::input::keyboard::FlxKeyboard tmp4 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(50)
		bool tmp5 = tmp4->checkKeyArrayState(Array_obj< int >::__new().Add((int)37),(int)1);		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(50)
		if ((tmp5)){
			HX_STACK_LINE(52)
			::flixel::animation::FlxAnimationController tmp6 = this->animation;		HX_STACK_VAR(tmp6,"tmp6");
			HX_STACK_LINE(52)
			tmp6->play(HX_HCSTRING("LefDod","\xcc","\xc0","\x37","\xce"),null(),null(),null());
		}
		HX_STACK_LINE(55)
		::flixel::input::keyboard::FlxKeyboard tmp6 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(55)
		bool tmp7 = tmp6->checkKeyArrayState(Array_obj< int >::__new().Add((int)39),(int)1);		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(55)
		if ((tmp7)){
			HX_STACK_LINE(58)
			::flixel::animation::FlxAnimationController tmp8 = this->animation;		HX_STACK_VAR(tmp8,"tmp8");
			HX_STACK_LINE(58)
			tmp8->play(HX_HCSTRING("RigDod","\xa9","\x73","\x9f","\x82"),null(),null(),null());
		}
		HX_STACK_LINE(62)
		::flixel::input::keyboard::FlxKeyboard tmp8 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(62)
		bool tmp9 = tmp8->checkKeyArrayState(Array_obj< int >::__new().Add((int)90),(int)1);		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(62)
		if ((tmp9)){
			HX_STACK_LINE(65)
			::flixel::animation::FlxAnimationController tmp10 = this->animation;		HX_STACK_VAR(tmp10,"tmp10");
			HX_STACK_LINE(65)
			tmp10->play(HX_HCSTRING("LeftPunch","\x07","\x8c","\x3d","\x81"),null(),null(),null());
		}
		HX_STACK_LINE(69)
		Float tmp10 = this->health;		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(69)
		bool tmp11 = (tmp10 <= (int)0);		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(69)
		if ((tmp11)){
			HX_STACK_LINE(72)
			::BoxerLose tmp12 = ::BoxerLose_obj::__new(null());		HX_STACK_VAR(tmp12,"tmp12");
			HX_STACK_LINE(72)
			::flixel::FlxState nextState = tmp12;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(72)
			::flixel::FlxGame tmp13 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp13,"tmp13");
			HX_STACK_LINE(72)
			::flixel::FlxState tmp14 = nextState;		HX_STACK_VAR(tmp14,"tmp14");
			HX_STACK_LINE(72)
			bool tmp15 = tmp13->_state->switchTo(tmp14);		HX_STACK_VAR(tmp15,"tmp15");
			HX_STACK_LINE(72)
			if ((tmp15)){
				HX_STACK_LINE(72)
				::flixel::FlxGame tmp16 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp16,"tmp16");
				HX_STACK_LINE(72)
				tmp16->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(78)
		Float tmp12 = elapsed;		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(78)
		this->super::update(tmp12);
	}
return null();
}


Void PlayerBoxer_obj::stateChange( ::String anim){
{
		HX_STACK_FRAME("PlayerBoxer","stateChange",0x1e3f536a,"PlayerBoxer.stateChange","PlayerBoxer.hx",86,0x1783ab27)
		HX_STACK_THIS(this)
		HX_STACK_ARG(anim,"anim")
		HX_STACK_LINE(87)
		::String tmp = anim;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(87)
		::String _switch_1 = (tmp);
		if (  ( _switch_1==HX_HCSTRING("RightPunch","\x52","\xa8","\x00","\xc4"))){
			HX_STACK_LINE(89)
			::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(89)
			tmp1->play(HX_HCSTRING("Idle","\x34","\xd3","\x8c","\x30"),null(),null(),null());
			HX_STACK_LINE(90)
			::Book tmp2 = this->book;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(90)
			hx::SubEq(tmp2->health,(int)10);
		}
		else if (  ( _switch_1==HX_HCSTRING("Dod","\x59","\xfa","\x33","\x00"))){
			HX_STACK_LINE(92)
			::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(92)
			tmp1->play(HX_HCSTRING("Idle","\x34","\xd3","\x8c","\x30"),null(),null(),null());
		}
		else if (  ( _switch_1==HX_HCSTRING("LeftPunch","\x07","\x8c","\x3d","\x81"))){
			HX_STACK_LINE(94)
			::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(94)
			tmp1->play(HX_HCSTRING("Idle","\x34","\xd3","\x8c","\x30"),null(),null(),null());
			HX_STACK_LINE(95)
			::Book tmp2 = this->book;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(95)
			hx::SubEq(tmp2->health,(int)10);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayerBoxer_obj,stateChange,(void))


PlayerBoxer_obj::PlayerBoxer_obj()
{
}

void PlayerBoxer_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(PlayerBoxer);
	HX_MARK_MEMBER_NAME(book,"book");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void PlayerBoxer_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(book,"book");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic PlayerBoxer_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"book") ) { return book; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"stateChange") ) { return stateChange_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PlayerBoxer_obj::__SetField(const ::String &inName,const Dynamic &inValue,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"book") ) { book=inValue.Cast< ::Book >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PlayerBoxer_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_HCSTRING("book","\x29","\x84","\x1b","\x41"));
	super::__GetFields(outFields);
};

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::Book*/ ,(int)offsetof(PlayerBoxer_obj,book),HX_HCSTRING("book","\x29","\x84","\x1b","\x41")},
	{ hx::fsUnknown, 0, null()}
};
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("book","\x29","\x84","\x1b","\x41"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	HX_HCSTRING("stateChange","\x61","\xde","\xfb","\x31"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PlayerBoxer_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PlayerBoxer_obj::__mClass,"__mClass");
};

#endif

hx::Class PlayerBoxer_obj::__mClass;

void PlayerBoxer_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("PlayerBoxer","\xf7","\x2b","\x10","\xa2");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< PlayerBoxer_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

