#include <hxcpp.h>

#ifndef INCLUDED_PlayerBullet
#include <PlayerBullet.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_math_FlxPoint
#include <flixel/math/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxPooled
#include <flixel/util/IFlxPooled.h>
#endif

Void PlayerBullet_obj::__construct(Dynamic x,Dynamic y)
{
HX_STACK_FRAME("PlayerBullet","new",0x1b8eab15,"PlayerBullet.new","PlayerBullet.hx",9,0x5e420dfb)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
{
	HX_STACK_LINE(10)
	Dynamic tmp = x;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(10)
	Dynamic tmp1 = y;		HX_STACK_VAR(tmp1,"tmp1");
	HX_STACK_LINE(10)
	super::__construct(tmp,tmp1,HX_HCSTRING("assets/images/Level2/Playerbullet.png","\x14","\x65","\xc0","\x1d"));
	HX_STACK_LINE(11)
	::flixel::math::FlxPoint tmp2 = this->velocity;		HX_STACK_VAR(tmp2,"tmp2");
	HX_STACK_LINE(11)
	tmp2->set_y((int)-180);
}
;
	return null();
}

//PlayerBullet_obj::~PlayerBullet_obj() { }

Dynamic PlayerBullet_obj::__CreateEmpty() { return  new PlayerBullet_obj; }
hx::ObjectPtr< PlayerBullet_obj > PlayerBullet_obj::__new(Dynamic x,Dynamic y)
{  hx::ObjectPtr< PlayerBullet_obj > _result_ = new PlayerBullet_obj();
	_result_->__construct(x,y);
	return _result_;}

Dynamic PlayerBullet_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PlayerBullet_obj > _result_ = new PlayerBullet_obj();
	_result_->__construct(inArgs[0],inArgs[1]);
	return _result_;}


PlayerBullet_obj::PlayerBullet_obj()
{
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PlayerBullet_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PlayerBullet_obj::__mClass,"__mClass");
};

#endif

hx::Class PlayerBullet_obj::__mClass;

void PlayerBullet_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("PlayerBullet","\xa3","\x7c","\x94","\x98");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(0 /* sMemberFields */);
	__mClass->mCanCast = hx::TCanCast< PlayerBullet_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

