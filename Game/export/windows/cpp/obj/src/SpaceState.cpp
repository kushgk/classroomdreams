#include <hxcpp.h>

#include "hxMath.h"
#ifndef INCLUDED_ChemState
#include <ChemState.h>
#endif
#ifndef INCLUDED_EnemyBullet
#include <EnemyBullet.h>
#endif
#ifndef INCLUDED_Integral
#include <Integral.h>
#endif
#ifndef INCLUDED_PlayerBullet
#include <PlayerBullet.h>
#endif
#ifndef INCLUDED_PlayerShip
#include <PlayerShip.h>
#endif
#ifndef INCLUDED_SpaceLose
#include <SpaceLose.h>
#endif
#ifndef INCLUDED_SpaceState
#include <SpaceState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_FlxKeyManager
#include <flixel/input/FlxKeyManager.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInputManager
#include <flixel/input/IFlxInputManager.h>
#endif
#ifndef INCLUDED_flixel_input_keyboard_FlxKeyboard
#include <flixel/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSoundGroup
#include <flixel/system/FlxSoundGroup.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCollision
#include <flixel/util/FlxCollision.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void SpaceState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("SpaceState","new",0xf7b311fd,"SpaceState.new","SpaceState.hx",11,0x048dfc13)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(11)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(11)
	super::__construct(tmp);
}
;
	return null();
}

//SpaceState_obj::~SpaceState_obj() { }

Dynamic SpaceState_obj::__CreateEmpty() { return  new SpaceState_obj; }
hx::ObjectPtr< SpaceState_obj > SpaceState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< SpaceState_obj > _result_ = new SpaceState_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic SpaceState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SpaceState_obj > _result_ = new SpaceState_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void SpaceState_obj::create( ){
{
		HX_STACK_FRAME("SpaceState","create",0x88b5457f,"SpaceState.create","SpaceState.hx",20,0x048dfc13)
		HX_STACK_THIS(this)
		HX_STACK_LINE(21)
		this->super::create();
		HX_STACK_LINE(23)
		::flixel::FlxSprite tmp = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/Level2/Stage2.png","\x65","\xa4","\x9d","\x69"));		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(23)
		this->add(tmp);
		HX_STACK_LINE(25)
		::flixel::group::FlxTypedGroup tmp1 = ::flixel::group::FlxTypedGroup_obj::__new((int)80);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(25)
		this->playerBullets = tmp1;
		HX_STACK_LINE(26)
		::flixel::group::FlxTypedGroup tmp2 = ::flixel::group::FlxTypedGroup_obj::__new((int)80);		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(26)
		this->enemyBullets = tmp2;
		HX_STACK_LINE(27)
		::flixel::group::FlxTypedGroup tmp3 = ::flixel::group::FlxTypedGroup_obj::__new((int)32);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(27)
		this->enemies = tmp3;
		HX_STACK_LINE(28)
		{
			HX_STACK_LINE(28)
			int tmp4 = (int)-1;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(28)
			int _g = tmp4;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(28)
			while((true)){
				HX_STACK_LINE(28)
				bool tmp5 = (_g < (int)2);		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(28)
				bool tmp6 = !(tmp5);		HX_STACK_VAR(tmp6,"tmp6");
				HX_STACK_LINE(28)
				if ((tmp6)){
					HX_STACK_LINE(28)
					break;
				}
				HX_STACK_LINE(28)
				int tmp7 = (_g)++;		HX_STACK_VAR(tmp7,"tmp7");
				HX_STACK_LINE(28)
				int j = tmp7;		HX_STACK_VAR(j,"j");
				HX_STACK_LINE(29)
				{
					HX_STACK_LINE(29)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(29)
					while((true)){
						HX_STACK_LINE(29)
						bool tmp8 = (_g1 < (int)8);		HX_STACK_VAR(tmp8,"tmp8");
						HX_STACK_LINE(29)
						bool tmp9 = !(tmp8);		HX_STACK_VAR(tmp9,"tmp9");
						HX_STACK_LINE(29)
						if ((tmp9)){
							HX_STACK_LINE(29)
							break;
						}
						HX_STACK_LINE(29)
						int tmp10 = (_g1)++;		HX_STACK_VAR(tmp10,"tmp10");
						HX_STACK_LINE(29)
						int i = tmp10;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(30)
						::flixel::group::FlxTypedGroup tmp11 = this->enemies;		HX_STACK_VAR(tmp11,"tmp11");
						HX_STACK_LINE(30)
						int tmp12 = ((int)32 * i);		HX_STACK_VAR(tmp12,"tmp12");
						HX_STACK_LINE(30)
						int tmp13 = ((int)32 * j);		HX_STACK_VAR(tmp13,"tmp13");
						HX_STACK_LINE(30)
						::Integral tmp14 = ::Integral_obj::__new(tmp12,tmp13);		HX_STACK_VAR(tmp14,"tmp14");
						HX_STACK_LINE(30)
						tmp11->add(tmp14);
					}
				}
			}
		}
		HX_STACK_LINE(33)
		::PlayerShip tmp4 = ::PlayerShip_obj::__new(null(),null(),null(),null());		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(33)
		this->player = tmp4;
		HX_STACK_LINE(34)
		::PlayerShip tmp5 = this->player;		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(34)
		this->add(tmp5);
		HX_STACK_LINE(35)
		::flixel::group::FlxTypedGroup tmp6 = this->playerBullets;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(35)
		this->add(tmp6);
		HX_STACK_LINE(36)
		::flixel::group::FlxTypedGroup tmp7 = this->enemyBullets;		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(36)
		this->add(tmp7);
		HX_STACK_LINE(37)
		::flixel::group::FlxTypedGroup tmp8 = this->enemies;		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(37)
		this->add(tmp8);
		HX_STACK_LINE(38)
		this->timer = (int)0;
		HX_STACK_LINE(39)
		::flixel::_system::frontEnds::SoundFrontEnd tmp9 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(39)
		tmp9->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/drowsy1.wav","\x1d","\x0f","\xeb","\x32"),(int)1,true,null());
	}
return null();
}


Void SpaceState_obj::update( Float elapsed){
{
		HX_STACK_FRAME("SpaceState","update",0x93ab648c,"SpaceState.update","SpaceState.hx",42,0x048dfc13)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(44)
		::flixel::_system::frontEnds::SoundFrontEnd tmp = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(44)
		::flixel::_system::FlxSound tmp1 = tmp->__Field(HX_HCSTRING("music","\xa5","\xd0","\x5a","\x10"), hx::paccDynamic );		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(44)
		bool tmp2 = (tmp1 == null());		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(44)
		if ((tmp2)){
			HX_STACK_LINE(46)
			::flixel::_system::frontEnds::SoundFrontEnd tmp3 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(46)
			tmp3->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/drowsy1.wav","\x1d","\x0f","\xeb","\x32"),(int)1,true,null());
		}
		HX_STACK_LINE(49)
		::flixel::group::FlxTypedGroup tmp3 = this->enemies;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(49)
		int tmp4 = tmp3->countLiving();		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(49)
		bool tmp5 = (tmp4 == (int)0);		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(49)
		bool tmp6 = !(tmp5);		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(49)
		bool tmp7;		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(49)
		if ((tmp6)){
			HX_STACK_LINE(49)
			::flixel::group::FlxTypedGroup tmp8 = this->enemies;		HX_STACK_VAR(tmp8,"tmp8");
			HX_STACK_LINE(49)
			::flixel::group::FlxTypedGroup tmp9 = tmp8;		HX_STACK_VAR(tmp9,"tmp9");
			HX_STACK_LINE(49)
			int tmp10 = tmp9->countLiving();		HX_STACK_VAR(tmp10,"tmp10");
			HX_STACK_LINE(49)
			int tmp11 = tmp10;		HX_STACK_VAR(tmp11,"tmp11");
			HX_STACK_LINE(49)
			tmp7 = (tmp11 == (int)-1);
		}
		else{
			HX_STACK_LINE(49)
			tmp7 = true;
		}
		HX_STACK_LINE(49)
		if ((tmp7)){
			HX_STACK_LINE(50)
			::ChemState tmp8 = ::ChemState_obj::__new(null());		HX_STACK_VAR(tmp8,"tmp8");
			HX_STACK_LINE(50)
			::flixel::FlxState nextState = tmp8;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(50)
			::flixel::FlxGame tmp9 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp9,"tmp9");
			HX_STACK_LINE(50)
			::flixel::FlxState tmp10 = nextState;		HX_STACK_VAR(tmp10,"tmp10");
			HX_STACK_LINE(50)
			bool tmp11 = tmp9->_state->switchTo(tmp10);		HX_STACK_VAR(tmp11,"tmp11");
			HX_STACK_LINE(50)
			if ((tmp11)){
				HX_STACK_LINE(50)
				::flixel::FlxGame tmp12 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp12,"tmp12");
				HX_STACK_LINE(50)
				tmp12->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(53)
		(this->timer)--;
		HX_STACK_LINE(54)
		::flixel::input::keyboard::FlxKeyboard tmp8 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(54)
		bool tmp9 = tmp8->checkKeyArrayState(Array_obj< int >::__new().Add((int)32),(int)1);		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(54)
		bool tmp10;		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(54)
		if ((tmp9)){
			HX_STACK_LINE(54)
			Float tmp11 = this->timer;		HX_STACK_VAR(tmp11,"tmp11");
			HX_STACK_LINE(54)
			Float tmp12 = tmp11;		HX_STACK_VAR(tmp12,"tmp12");
			HX_STACK_LINE(54)
			tmp10 = (tmp12 < (int)0);
		}
		else{
			HX_STACK_LINE(54)
			tmp10 = false;
		}
		HX_STACK_LINE(54)
		if ((tmp10)){
			HX_STACK_LINE(56)
			::flixel::group::FlxTypedGroup tmp11 = this->playerBullets;		HX_STACK_VAR(tmp11,"tmp11");
			HX_STACK_LINE(56)
			::PlayerShip tmp12 = this->player;		HX_STACK_VAR(tmp12,"tmp12");
			HX_STACK_LINE(56)
			Float tmp13 = tmp12->x;		HX_STACK_VAR(tmp13,"tmp13");
			HX_STACK_LINE(56)
			::PlayerShip tmp14 = this->player;		HX_STACK_VAR(tmp14,"tmp14");
			HX_STACK_LINE(56)
			Float tmp15 = tmp14->get_width();		HX_STACK_VAR(tmp15,"tmp15");
			HX_STACK_LINE(56)
			Float tmp16 = (Float(tmp15) / Float((int)2));		HX_STACK_VAR(tmp16,"tmp16");
			HX_STACK_LINE(56)
			Float tmp17 = (tmp13 + tmp16);		HX_STACK_VAR(tmp17,"tmp17");
			HX_STACK_LINE(56)
			::PlayerShip tmp18 = this->player;		HX_STACK_VAR(tmp18,"tmp18");
			HX_STACK_LINE(56)
			Float tmp19 = tmp18->y;		HX_STACK_VAR(tmp19,"tmp19");
			HX_STACK_LINE(56)
			::PlayerBullet tmp20 = ::PlayerBullet_obj::__new(tmp17,tmp19);		HX_STACK_VAR(tmp20,"tmp20");
			HX_STACK_LINE(56)
			tmp11->add(tmp20);
			HX_STACK_LINE(57)
			this->timer = (int)20;
		}
		HX_STACK_LINE(60)
		::flixel::group::FlxTypedGroup tmp11 = this->enemies;		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(60)
		::flixel::FlxBasic tmp12 = tmp11->getRandom(null(),null());		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(60)
		this->enemyShoot(tmp12);
		HX_STACK_LINE(62)
		::flixel::group::FlxTypedGroup tmp13 = this->enemies;		HX_STACK_VAR(tmp13,"tmp13");
		HX_STACK_LINE(62)
		::flixel::group::FlxTypedGroup tmp14 = this->playerBullets;		HX_STACK_VAR(tmp14,"tmp14");
		HX_STACK_LINE(62)
		Dynamic tmp15 = this->killEnemy_dyn();		HX_STACK_VAR(tmp15,"tmp15");
		HX_STACK_LINE(62)
		::flixel::FlxG_obj::overlap(tmp13,tmp14,tmp15,null());
		HX_STACK_LINE(63)
		::PlayerShip tmp16 = this->player;		HX_STACK_VAR(tmp16,"tmp16");
		HX_STACK_LINE(63)
		::flixel::group::FlxTypedGroup tmp17 = this->enemyBullets;		HX_STACK_VAR(tmp17,"tmp17");
		HX_STACK_LINE(63)
		Dynamic tmp18 = this->killPlayer_dyn();		HX_STACK_VAR(tmp18,"tmp18");
		HX_STACK_LINE(63)
		::flixel::FlxG_obj::overlap(tmp16,tmp17,tmp18,null());
		HX_STACK_LINE(64)
		Float tmp19 = elapsed;		HX_STACK_VAR(tmp19,"tmp19");
		HX_STACK_LINE(64)
		this->super::update(tmp19);
	}
return null();
}


Void SpaceState_obj::killEnemy( Dynamic enemy,Dynamic bullet){
{
		HX_STACK_FRAME("SpaceState","killEnemy",0x1a491b27,"SpaceState.killEnemy","SpaceState.hx",67,0x048dfc13)
		HX_STACK_THIS(this)
		HX_STACK_ARG(enemy,"enemy")
		HX_STACK_ARG(bullet,"bullet")
		HX_STACK_LINE(68)
		bullet->__Field(HX_HCSTRING("destroy","\xfa","\x2c","\x86","\x24"), hx::paccDynamic )();
		HX_STACK_LINE(69)
		enemy->__Field(HX_HCSTRING("die","\x40","\x3d","\x4c","\x00"), hx::paccDynamic )();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(SpaceState_obj,killEnemy,(void))

Void SpaceState_obj::killPlayer( ::flixel::FlxSprite play,::flixel::FlxSprite bullet){
{
		HX_STACK_FRAME("SpaceState","killPlayer",0x2232a5a2,"SpaceState.killPlayer","SpaceState.hx",72,0x048dfc13)
		HX_STACK_THIS(this)
		HX_STACK_ARG(play,"play")
		HX_STACK_ARG(bullet,"bullet")
		HX_STACK_LINE(73)
		bool tmp;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(73)
		{
			HX_STACK_LINE(73)
			int AlphaTolerance = (int)255;		HX_STACK_VAR(AlphaTolerance,"AlphaTolerance");
			HX_STACK_LINE(73)
			::flixel::FlxSprite tmp1 = play;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(73)
			::flixel::FlxSprite tmp2 = bullet;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(73)
			int tmp3 = AlphaTolerance;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(73)
			tmp = ::flixel::util::FlxCollision_obj::pixelPerfectCheck(tmp1,tmp2,tmp3,null());
		}
		HX_STACK_LINE(73)
		if ((tmp)){
			HX_STACK_LINE(74)
			play->destroy();
			HX_STACK_LINE(75)
			bullet->destroy();
			HX_STACK_LINE(76)
			{
				HX_STACK_LINE(76)
				::SpaceLose tmp1 = ::SpaceLose_obj::__new(null());		HX_STACK_VAR(tmp1,"tmp1");
				HX_STACK_LINE(76)
				::flixel::FlxState nextState = tmp1;		HX_STACK_VAR(nextState,"nextState");
				HX_STACK_LINE(76)
				::flixel::FlxGame tmp2 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp2,"tmp2");
				HX_STACK_LINE(76)
				::flixel::FlxState tmp3 = nextState;		HX_STACK_VAR(tmp3,"tmp3");
				HX_STACK_LINE(76)
				bool tmp4 = tmp2->_state->switchTo(tmp3);		HX_STACK_VAR(tmp4,"tmp4");
				HX_STACK_LINE(76)
				if ((tmp4)){
					HX_STACK_LINE(76)
					::flixel::FlxGame tmp5 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp5,"tmp5");
					HX_STACK_LINE(76)
					tmp5->_requestedState = nextState;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(SpaceState_obj,killPlayer,(void))

Void SpaceState_obj::enemyShoot( ::flixel::FlxBasic enem){
{
		HX_STACK_FRAME("SpaceState","enemyShoot",0x5c95767a,"SpaceState.enemyShoot","SpaceState.hx",81,0x048dfc13)
		HX_STACK_THIS(this)
		HX_STACK_ARG(enem,"enem")
		HX_STACK_LINE(82)
		::Integral enemy = ((::Integral)(enem));		HX_STACK_VAR(enemy,"enemy");
		HX_STACK_LINE(83)
		Float tmp = ::Math_obj::random();		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(83)
		Float tmp1 = ((Float)0.0374);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(83)
		Float tmp2 = (Float(tmp1) / Float((int)2));		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(83)
		bool tmp3 = (tmp < tmp2);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(83)
		if ((tmp3)){
			HX_STACK_LINE(84)
			::flixel::group::FlxTypedGroup tmp4 = this->enemyBullets;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(84)
			Float tmp5 = enemy->x;		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(84)
			Float tmp6 = enemy->get_width();		HX_STACK_VAR(tmp6,"tmp6");
			HX_STACK_LINE(84)
			Float tmp7 = (Float(tmp6) / Float((int)2));		HX_STACK_VAR(tmp7,"tmp7");
			HX_STACK_LINE(84)
			Float tmp8 = (tmp5 + tmp7);		HX_STACK_VAR(tmp8,"tmp8");
			HX_STACK_LINE(84)
			Float tmp9 = enemy->y;		HX_STACK_VAR(tmp9,"tmp9");
			HX_STACK_LINE(84)
			Float tmp10 = enemy->get_height();		HX_STACK_VAR(tmp10,"tmp10");
			HX_STACK_LINE(84)
			Float tmp11 = (tmp9 + tmp10);		HX_STACK_VAR(tmp11,"tmp11");
			HX_STACK_LINE(84)
			::EnemyBullet tmp12 = ::EnemyBullet_obj::__new(tmp8,tmp11);		HX_STACK_VAR(tmp12,"tmp12");
			HX_STACK_LINE(84)
			tmp4->add(tmp12);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SpaceState_obj,enemyShoot,(void))


SpaceState_obj::SpaceState_obj()
{
}

void SpaceState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(SpaceState);
	HX_MARK_MEMBER_NAME(player,"player");
	HX_MARK_MEMBER_NAME(enemies,"enemies");
	HX_MARK_MEMBER_NAME(playerBullets,"playerBullets");
	HX_MARK_MEMBER_NAME(enemyBullets,"enemyBullets");
	HX_MARK_MEMBER_NAME(timer,"timer");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void SpaceState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(player,"player");
	HX_VISIT_MEMBER_NAME(enemies,"enemies");
	HX_VISIT_MEMBER_NAME(playerBullets,"playerBullets");
	HX_VISIT_MEMBER_NAME(enemyBullets,"enemyBullets");
	HX_VISIT_MEMBER_NAME(timer,"timer");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic SpaceState_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"timer") ) { return timer; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enemies") ) { return enemies; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"killEnemy") ) { return killEnemy_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"killPlayer") ) { return killPlayer_dyn(); }
		if (HX_FIELD_EQ(inName,"enemyShoot") ) { return enemyShoot_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"enemyBullets") ) { return enemyBullets; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"playerBullets") ) { return playerBullets; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SpaceState_obj::__SetField(const ::String &inName,const Dynamic &inValue,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"timer") ) { timer=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::PlayerShip >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enemies") ) { enemies=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"enemyBullets") ) { enemyBullets=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"playerBullets") ) { playerBullets=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void SpaceState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"));
	outFields->push(HX_HCSTRING("enemies","\xa6","\x68","\x0e","\xd3"));
	outFields->push(HX_HCSTRING("playerBullets","\x90","\x42","\x68","\xa1"));
	outFields->push(HX_HCSTRING("enemyBullets","\x89","\xd6","\x5b","\xfe"));
	outFields->push(HX_HCSTRING("timer","\xc5","\xbf","\x35","\x10"));
	super::__GetFields(outFields);
};

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::PlayerShip*/ ,(int)offsetof(SpaceState_obj,player),HX_HCSTRING("player","\x61","\xeb","\xb8","\x37")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(SpaceState_obj,enemies),HX_HCSTRING("enemies","\xa6","\x68","\x0e","\xd3")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(SpaceState_obj,playerBullets),HX_HCSTRING("playerBullets","\x90","\x42","\x68","\xa1")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(SpaceState_obj,enemyBullets),HX_HCSTRING("enemyBullets","\x89","\xd6","\x5b","\xfe")},
	{hx::fsFloat,(int)offsetof(SpaceState_obj,timer),HX_HCSTRING("timer","\xc5","\xbf","\x35","\x10")},
	{ hx::fsUnknown, 0, null()}
};
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"),
	HX_HCSTRING("enemies","\xa6","\x68","\x0e","\xd3"),
	HX_HCSTRING("playerBullets","\x90","\x42","\x68","\xa1"),
	HX_HCSTRING("enemyBullets","\x89","\xd6","\x5b","\xfe"),
	HX_HCSTRING("timer","\xc5","\xbf","\x35","\x10"),
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	HX_HCSTRING("killEnemy","\xca","\x45","\xd9","\x5d"),
	HX_HCSTRING("killPlayer","\x9f","\xc9","\xc7","\xfc"),
	HX_HCSTRING("enemyShoot","\x77","\x9a","\x2a","\x37"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SpaceState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SpaceState_obj::__mClass,"__mClass");
};

#endif

hx::Class SpaceState_obj::__mClass;

void SpaceState_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("SpaceState","\x8b","\x4f","\xf1","\xdb");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< SpaceState_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

