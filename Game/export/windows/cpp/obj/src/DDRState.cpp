#include <hxcpp.h>

#include "hxMath.h"
#ifndef INCLUDED_DDRLose
#include <DDRLose.h>
#endif
#ifndef INCLUDED_DDRState
#include <DDRState.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_FlxKeyManager
#include <flixel/input/FlxKeyManager.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInputManager
#include <flixel/input/IFlxInputManager.h>
#endif
#ifndef INCLUDED_flixel_input_keyboard_FlxKeyboard
#include <flixel/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSoundGroup
#include <flixel/system/FlxSoundGroup.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void DDRState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("DDRState","new",0x0224d451,"DDRState.new","DDRState.hx",12,0x1943e03f)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(12)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(12)
	super::__construct(tmp);
}
;
	return null();
}

//DDRState_obj::~DDRState_obj() { }

Dynamic DDRState_obj::__CreateEmpty() { return  new DDRState_obj; }
hx::ObjectPtr< DDRState_obj > DDRState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< DDRState_obj > _result_ = new DDRState_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic DDRState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< DDRState_obj > _result_ = new DDRState_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void DDRState_obj::create( ){
{
		HX_STACK_FRAME("DDRState","create",0x4dceafab,"DDRState.create","DDRState.hx",23,0x1943e03f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(24)
		::flixel::FlxSprite tmp = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/Level4/Stage1.png","\x22","\xeb","\xb5","\x59"));		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(24)
		this->add(tmp);
		HX_STACK_LINE(26)
		this->beatCounter = (int)0;
		HX_STACK_LINE(27)
		this->failure = (int)0;
		HX_STACK_LINE(29)
		::flixel::group::FlxTypedGroup tmp1 = ::flixel::group::FlxTypedGroup_obj::__new((int)3);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(29)
		this->dancers = tmp1;
		HX_STACK_LINE(30)
		::flixel::group::FlxTypedGroup tmp2 = ::flixel::group::FlxTypedGroup_obj::__new((int)999);		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(30)
		this->prompts = tmp2;
		HX_STACK_LINE(31)
		::flixel::group::FlxTypedGroup tmp3 = ::flixel::group::FlxTypedGroup_obj::__new((int)999);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(31)
		this->outputs = tmp3;
		HX_STACK_LINE(33)
		int tmp4 = (int)128;		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(33)
		::flixel::FlxSprite tmp5 = ::flixel::FlxSprite_obj::__new(tmp4,(int)120,null());		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(33)
		this->player = tmp5;
		HX_STACK_LINE(34)
		::flixel::FlxSprite tmp6 = this->player;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(34)
		tmp6->loadGraphic(HX_HCSTRING("assets/images/Level4/DancingSS.png","\xd7","\xa4","\x3c","\xc9"),true,(int)64,(int)64,null(),null());
		HX_STACK_LINE(35)
		::flixel::FlxSprite tmp7 = this->player;		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(35)
		tmp7->animation->add(HX_HCSTRING("Down","\x82","\x24","\x47","\x2d"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)6,true,null(),null());
		HX_STACK_LINE(36)
		::flixel::FlxSprite tmp8 = this->player;		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(36)
		tmp8->animation->add(HX_HCSTRING("Left","\x27","\x34","\x89","\x32"),Array_obj< int >::__new().Add((int)2).Add((int)3),(int)6,true,null(),null());
		HX_STACK_LINE(37)
		::flixel::FlxSprite tmp9 = this->player;		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(37)
		tmp9->animation->add(HX_HCSTRING("Right","\xbc","\x7b","\x91","\x7c"),Array_obj< int >::__new().Add((int)4).Add((int)5),(int)6,true,null(),null());
		HX_STACK_LINE(38)
		::flixel::FlxSprite tmp10 = this->player;		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(38)
		tmp10->animation->add(HX_HCSTRING("Up","\x7b","\x4a","\x00","\x00"),Array_obj< int >::__new().Add((int)6).Add((int)7),(int)6,true,null(),null());
		HX_STACK_LINE(39)
		::flixel::group::FlxTypedGroup tmp11 = this->dancers;		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(39)
		::flixel::FlxSprite tmp12 = this->player;		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(39)
		tmp11->add(tmp12);
		HX_STACK_LINE(41)
		int tmp13 = (int)64;		HX_STACK_VAR(tmp13,"tmp13");
		HX_STACK_LINE(41)
		::flixel::FlxSprite tmp14 = ::flixel::FlxSprite_obj::__new(tmp13,(int)88,null());		HX_STACK_VAR(tmp14,"tmp14");
		HX_STACK_LINE(41)
		this->napoleon = tmp14;
		HX_STACK_LINE(42)
		::flixel::FlxSprite tmp15 = this->napoleon;		HX_STACK_VAR(tmp15,"tmp15");
		HX_STACK_LINE(42)
		tmp15->loadGraphic(HX_HCSTRING("assets/images/Level4/NapoleonSS.png","\xd9","\x75","\x73","\x99"),true,(int)64,(int)64,null(),null());
		HX_STACK_LINE(43)
		::flixel::FlxSprite tmp16 = this->napoleon;		HX_STACK_VAR(tmp16,"tmp16");
		HX_STACK_LINE(43)
		tmp16->animation->add(HX_HCSTRING("Down","\x82","\x24","\x47","\x2d"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)6,true,null(),null());
		HX_STACK_LINE(44)
		::flixel::FlxSprite tmp17 = this->napoleon;		HX_STACK_VAR(tmp17,"tmp17");
		HX_STACK_LINE(44)
		tmp17->animation->add(HX_HCSTRING("Left","\x27","\x34","\x89","\x32"),Array_obj< int >::__new().Add((int)2).Add((int)3),(int)6,true,null(),null());
		HX_STACK_LINE(45)
		::flixel::FlxSprite tmp18 = this->napoleon;		HX_STACK_VAR(tmp18,"tmp18");
		HX_STACK_LINE(45)
		tmp18->animation->add(HX_HCSTRING("Right","\xbc","\x7b","\x91","\x7c"),Array_obj< int >::__new().Add((int)4).Add((int)5),(int)6,true,null(),null());
		HX_STACK_LINE(46)
		::flixel::FlxSprite tmp19 = this->napoleon;		HX_STACK_VAR(tmp19,"tmp19");
		HX_STACK_LINE(46)
		tmp19->animation->add(HX_HCSTRING("Up","\x7b","\x4a","\x00","\x00"),Array_obj< int >::__new().Add((int)6).Add((int)7),(int)6,true,null(),null());
		HX_STACK_LINE(47)
		::flixel::group::FlxTypedGroup tmp20 = this->dancers;		HX_STACK_VAR(tmp20,"tmp20");
		HX_STACK_LINE(47)
		::flixel::FlxSprite tmp21 = this->napoleon;		HX_STACK_VAR(tmp21,"tmp21");
		HX_STACK_LINE(47)
		tmp20->add(tmp21);
		HX_STACK_LINE(49)
		int tmp22 = (int)192;		HX_STACK_VAR(tmp22,"tmp22");
		HX_STACK_LINE(49)
		::flixel::FlxSprite tmp23 = ::flixel::FlxSprite_obj::__new(tmp22,(int)88,null());		HX_STACK_VAR(tmp23,"tmp23");
		HX_STACK_LINE(49)
		this->lincoln = tmp23;
		HX_STACK_LINE(50)
		::flixel::FlxSprite tmp24 = this->lincoln;		HX_STACK_VAR(tmp24,"tmp24");
		HX_STACK_LINE(50)
		tmp24->loadGraphic(HX_HCSTRING("assets/images/Level4/LincolnSS.png","\xe6","\xce","\x95","\xa7"),true,(int)64,(int)64,null(),null());
		HX_STACK_LINE(51)
		::flixel::FlxSprite tmp25 = this->lincoln;		HX_STACK_VAR(tmp25,"tmp25");
		HX_STACK_LINE(51)
		tmp25->animation->add(HX_HCSTRING("Down","\x82","\x24","\x47","\x2d"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)6,true,null(),null());
		HX_STACK_LINE(52)
		::flixel::FlxSprite tmp26 = this->lincoln;		HX_STACK_VAR(tmp26,"tmp26");
		HX_STACK_LINE(52)
		tmp26->animation->add(HX_HCSTRING("Left","\x27","\x34","\x89","\x32"),Array_obj< int >::__new().Add((int)2).Add((int)3),(int)6,true,null(),null());
		HX_STACK_LINE(53)
		::flixel::FlxSprite tmp27 = this->lincoln;		HX_STACK_VAR(tmp27,"tmp27");
		HX_STACK_LINE(53)
		tmp27->animation->add(HX_HCSTRING("Right","\xbc","\x7b","\x91","\x7c"),Array_obj< int >::__new().Add((int)4).Add((int)5),(int)6,true,null(),null());
		HX_STACK_LINE(54)
		::flixel::FlxSprite tmp28 = this->lincoln;		HX_STACK_VAR(tmp28,"tmp28");
		HX_STACK_LINE(54)
		tmp28->animation->add(HX_HCSTRING("Up","\x7b","\x4a","\x00","\x00"),Array_obj< int >::__new().Add((int)6).Add((int)7),(int)6,true,null(),null());
		HX_STACK_LINE(55)
		::flixel::group::FlxTypedGroup tmp29 = this->dancers;		HX_STACK_VAR(tmp29,"tmp29");
		HX_STACK_LINE(55)
		::flixel::FlxSprite tmp30 = this->lincoln;		HX_STACK_VAR(tmp30,"tmp30");
		HX_STACK_LINE(55)
		tmp29->add(tmp30);
		HX_STACK_LINE(57)
		::flixel::FlxSprite tmp31 = ::flixel::FlxSprite_obj::__new((int)0,(int)4,HX_HCSTRING("assets/images/Level4/Track.png","\x12","\x03","\x2c","\x61"));		HX_STACK_VAR(tmp31,"tmp31");
		HX_STACK_LINE(57)
		this->add(tmp31);
		HX_STACK_LINE(58)
		::flixel::group::FlxTypedGroup tmp32 = this->dancers;		HX_STACK_VAR(tmp32,"tmp32");
		HX_STACK_LINE(58)
		this->add(tmp32);
		HX_STACK_LINE(59)
		::flixel::group::FlxTypedGroup tmp33 = this->prompts;		HX_STACK_VAR(tmp33,"tmp33");
		HX_STACK_LINE(59)
		this->add(tmp33);
		HX_STACK_LINE(60)
		::flixel::group::FlxTypedGroup tmp34 = this->outputs;		HX_STACK_VAR(tmp34,"tmp34");
		HX_STACK_LINE(60)
		this->add(tmp34);
		HX_STACK_LINE(62)
		::flixel::_system::frontEnds::SoundFrontEnd tmp35 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp35,"tmp35");
		HX_STACK_LINE(62)
		tmp35->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/hyper-partial4.wav","\x48","\x23","\xda","\xcc"),(int)1,true,null());
		HX_STACK_LINE(64)
		this->beats = Array_obj< ::String >::__new().Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("u","\x75","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("u","\x75","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("u","\x75","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("u","\x75","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("_","\x5f","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("d","\x64","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00")).Add(HX_HCSTRING("l","\x6c","\x00","\x00","\x00")).Add(HX_HCSTRING("r","\x72","\x00","\x00","\x00"));
	}
return null();
}


Void DDRState_obj::update( Float elapsed){
{
		HX_STACK_FRAME("DDRState","update",0x58c4ceb8,"DDRState.update","DDRState.hx",67,0x1943e03f)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(67)
		::DDRState _g = hx::ObjectPtr<OBJ_>(this);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(68)
		(this->beatCounter)++;
		HX_STACK_LINE(70)
		int tmp = this->failure;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(70)
		bool tmp1 = (tmp > (int)9);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(70)
		if ((tmp1)){
			HX_STACK_LINE(71)
			::DDRLose tmp2 = ::DDRLose_obj::__new(null());		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(71)
			::flixel::FlxState nextState = tmp2;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(71)
			::flixel::FlxGame tmp3 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(71)
			::flixel::FlxState tmp4 = nextState;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(71)
			bool tmp5 = tmp3->_state->switchTo(tmp4);		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(71)
			if ((tmp5)){
				HX_STACK_LINE(71)
				::flixel::FlxGame tmp6 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp6,"tmp6");
				HX_STACK_LINE(71)
				tmp6->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(74)
		int tmp2 = this->beats->length;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(74)
		bool tmp3 = (tmp2 == (int)0);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(74)
		if ((tmp3)){
			HX_STACK_LINE(75)
			::MenuState tmp4 = ::MenuState_obj::__new(null());		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(75)
			::flixel::FlxState nextState = tmp4;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(75)
			::flixel::FlxGame tmp5 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(75)
			::flixel::FlxState tmp6 = nextState;		HX_STACK_VAR(tmp6,"tmp6");
			HX_STACK_LINE(75)
			bool tmp7 = tmp5->_state->switchTo(tmp6);		HX_STACK_VAR(tmp7,"tmp7");
			HX_STACK_LINE(75)
			if ((tmp7)){
				HX_STACK_LINE(75)
				::flixel::FlxGame tmp8 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp8,"tmp8");
				HX_STACK_LINE(75)
				tmp8->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(78)
		::flixel::group::FlxTypedGroup tmp4 = this->prompts;		HX_STACK_VAR(tmp4,"tmp4");

		HX_BEGIN_LOCAL_FUNC_S1(hx::LocalFunc,_Function_1_1,::DDRState,_g)
		int __ArgCount() const { return 1; }
		Void run(::flixel::FlxBasic obj){
			HX_STACK_FRAME("*","_Function_1_1",0x5200ed37,"*._Function_1_1","DDRState.hx",78,0x1943e03f)
			HX_STACK_ARG(obj,"obj")
			{
				HX_STACK_LINE(79)
				::flixel::FlxSprite member = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(member,"member");
				HX_STACK_LINE(80)
				{
					HX_STACK_LINE(80)
					::flixel::FlxSprite _g1 = member;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(80)
					Float tmp5 = _g1->x;		HX_STACK_VAR(tmp5,"tmp5");
					HX_STACK_LINE(80)
					Float tmp6 = ((Float)6.4444444444444446);		HX_STACK_VAR(tmp6,"tmp6");
					HX_STACK_LINE(80)
					Float tmp7 = (tmp5 - tmp6);		HX_STACK_VAR(tmp7,"tmp7");
					HX_STACK_LINE(80)
					_g1->set_x(tmp7);
				}
				HX_STACK_LINE(81)
				Float tmp5 = member->x;		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(81)
				int tmp6 = (int)-1;		HX_STACK_VAR(tmp6,"tmp6");
				HX_STACK_LINE(81)
				Float tmp7 = member->get_width();		HX_STACK_VAR(tmp7,"tmp7");
				HX_STACK_LINE(81)
				Float tmp8 = (tmp6 * tmp7);		HX_STACK_VAR(tmp8,"tmp8");
				HX_STACK_LINE(81)
				bool tmp9 = (tmp5 < tmp8);		HX_STACK_VAR(tmp9,"tmp9");
				HX_STACK_LINE(81)
				if ((tmp9)){
					HX_STACK_LINE(82)
					::flixel::FlxSprite tmp10 = member;		HX_STACK_VAR(tmp10,"tmp10");
					HX_STACK_LINE(82)
					_g->prompts->remove(tmp10,null());
					HX_STACK_LINE(83)
					(_g->failure)++;
				}
			}
			return null();
		}
		HX_END_LOCAL_FUNC1((void))

		HX_STACK_LINE(78)
		tmp4->forEach( Dynamic(new _Function_1_1(_g)),null());
		HX_STACK_LINE(88)
		::flixel::group::FlxTypedGroup tmp5 = this->outputs;		HX_STACK_VAR(tmp5,"tmp5");

		HX_BEGIN_LOCAL_FUNC_S1(hx::LocalFunc,_Function_1_2,::DDRState,_g)
		int __ArgCount() const { return 1; }
		Void run(::flixel::FlxBasic obj){
			HX_STACK_FRAME("*","_Function_1_2",0x5200ed38,"*._Function_1_2","DDRState.hx",88,0x1943e03f)
			HX_STACK_ARG(obj,"obj")
			{
				HX_STACK_LINE(89)
				::flixel::FlxSprite member = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(member,"member");
				HX_STACK_LINE(90)
				{
					HX_STACK_LINE(90)
					::flixel::FlxSprite _g1 = member;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(90)
					Float tmp6 = _g1->x;		HX_STACK_VAR(tmp6,"tmp6");
					HX_STACK_LINE(90)
					Float tmp7 = ((Float)6.4444444444444446);		HX_STACK_VAR(tmp7,"tmp7");
					HX_STACK_LINE(90)
					Float tmp8 = (tmp6 - tmp7);		HX_STACK_VAR(tmp8,"tmp8");
					HX_STACK_LINE(90)
					_g1->set_x(tmp8);
				}
				HX_STACK_LINE(91)
				Float tmp6 = member->x;		HX_STACK_VAR(tmp6,"tmp6");
				HX_STACK_LINE(91)
				int tmp7 = (int)-1;		HX_STACK_VAR(tmp7,"tmp7");
				HX_STACK_LINE(91)
				Float tmp8 = member->get_width();		HX_STACK_VAR(tmp8,"tmp8");
				HX_STACK_LINE(91)
				Float tmp9 = (tmp7 * tmp8);		HX_STACK_VAR(tmp9,"tmp9");
				HX_STACK_LINE(91)
				bool tmp10 = (tmp6 < tmp9);		HX_STACK_VAR(tmp10,"tmp10");
				HX_STACK_LINE(91)
				if ((tmp10)){
					HX_STACK_LINE(92)
					::flixel::FlxSprite tmp11 = member;		HX_STACK_VAR(tmp11,"tmp11");
					HX_STACK_LINE(92)
					_g->prompts->remove(tmp11,null());
				}
			}
			return null();
		}
		HX_END_LOCAL_FUNC1((void))

		HX_STACK_LINE(88)
		tmp5->forEach( Dynamic(new _Function_1_2(_g)),null());
		HX_STACK_LINE(97)
		::flixel::input::keyboard::FlxKeyboard tmp6 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(97)
		bool tmp7 = tmp6->checkKeyArrayState(Array_obj< int >::__new().Add((int)38).Add((int)40).Add((int)39).Add((int)37),(int)2);		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(97)
		if ((tmp7)){
			HX_STACK_LINE(98)
			::flixel::group::FlxTypedGroup tmp8 = this->prompts;		HX_STACK_VAR(tmp8,"tmp8");
			HX_STACK_LINE(98)
			::flixel::FlxBasic tmp9 = tmp8->getFirstAlive();		HX_STACK_VAR(tmp9,"tmp9");
			HX_STACK_LINE(98)
			bool tmp10 = (tmp9 != null());		HX_STACK_VAR(tmp10,"tmp10");
			HX_STACK_LINE(98)
			if ((tmp10)){
				HX_STACK_LINE(99)
				bool correct = false;		HX_STACK_VAR(correct,"correct");
				HX_STACK_LINE(100)
				::flixel::group::FlxTypedGroup tmp11 = this->prompts;		HX_STACK_VAR(tmp11,"tmp11");
				HX_STACK_LINE(100)
				::flixel::FlxBasic tmp12 = tmp11->getFirstAlive();		HX_STACK_VAR(tmp12,"tmp12");
				HX_STACK_LINE(100)
				Array< ::Dynamic > first = Array_obj< ::Dynamic >::__new().Add(((::flixel::FlxSprite)(tmp12)));		HX_STACK_VAR(first,"first");
				HX_STACK_LINE(101)
				::flixel::group::FlxTypedGroup tmp13 = this->prompts;		HX_STACK_VAR(tmp13,"tmp13");

				HX_BEGIN_LOCAL_FUNC_S1(hx::LocalFunc,_Function_3_1,Array< ::Dynamic >,first)
				int __ArgCount() const { return 1; }
				Void run(::flixel::FlxBasic obj){
					HX_STACK_FRAME("*","_Function_3_1",0x520271b9,"*._Function_3_1","DDRState.hx",101,0x1943e03f)
					HX_STACK_ARG(obj,"obj")
					{
						HX_STACK_LINE(102)
						::flixel::FlxSprite member = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(member,"member");
						HX_STACK_LINE(103)
						bool tmp14 = (member->x < first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->x);		HX_STACK_VAR(tmp14,"tmp14");
						HX_STACK_LINE(103)
						if ((tmp14)){
							HX_STACK_LINE(104)
							first[(int)0] = member;
						}
					}
					return null();
				}
				HX_END_LOCAL_FUNC1((void))

				HX_STACK_LINE(101)
				tmp13->forEach( Dynamic(new _Function_3_1(first)),null());
				HX_STACK_LINE(107)
				{
					HX_STACK_LINE(107)
					int tmp14 = first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->animation->frameIndex;		HX_STACK_VAR(tmp14,"tmp14");
					HX_STACK_LINE(107)
					int _g1 = tmp14;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(107)
					int tmp15 = _g1;		HX_STACK_VAR(tmp15,"tmp15");
					HX_STACK_LINE(107)
					switch( (int)(tmp15)){
						case (int)1: {
							HX_STACK_LINE(109)
							::flixel::input::keyboard::FlxKeyboard tmp16 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp16,"tmp16");
							HX_STACK_LINE(109)
							bool tmp17 = tmp16->checkKeyArrayState(Array_obj< int >::__new().Add((int)38),(int)2);		HX_STACK_VAR(tmp17,"tmp17");
							HX_STACK_LINE(109)
							bool tmp18;		HX_STACK_VAR(tmp18,"tmp18");
							HX_STACK_LINE(109)
							if ((tmp17)){
								HX_STACK_LINE(109)
								Float tmp19 = (first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->x - (int)4);		HX_STACK_VAR(tmp19,"tmp19");
								HX_STACK_LINE(109)
								Float tmp20 = tmp19;		HX_STACK_VAR(tmp20,"tmp20");
								HX_STACK_LINE(109)
								Float tmp21 = tmp20;		HX_STACK_VAR(tmp21,"tmp21");
								HX_STACK_LINE(109)
								Float tmp22 = ::Math_obj::abs(tmp21);		HX_STACK_VAR(tmp22,"tmp22");
								HX_STACK_LINE(109)
								Float tmp23 = tmp22;		HX_STACK_VAR(tmp23,"tmp23");
								HX_STACK_LINE(109)
								tmp18 = (tmp23 < (int)20);
							}
							else{
								HX_STACK_LINE(109)
								tmp18 = false;
							}
							HX_STACK_LINE(109)
							if ((tmp18)){
								HX_STACK_LINE(110)
								correct = true;
								HX_STACK_LINE(111)
								::flixel::group::FlxTypedGroup tmp19 = this->dancers;		HX_STACK_VAR(tmp19,"tmp19");

								HX_BEGIN_LOCAL_FUNC_S0(hx::LocalFunc,_Function_6_1)
								int __ArgCount() const { return 1; }
								Void run(::flixel::FlxBasic obj){
									HX_STACK_FRAME("*","_Function_6_1",0x5204b87c,"*._Function_6_1","DDRState.hx",111,0x1943e03f)
									HX_STACK_ARG(obj,"obj")
									{
										HX_STACK_LINE(112)
										::flixel::FlxSprite dancer = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(dancer,"dancer");
										HX_STACK_LINE(113)
										dancer->animation->play(HX_HCSTRING("Up","\x7b","\x4a","\x00","\x00"),null(),null(),null());
									}
									return null();
								}
								HX_END_LOCAL_FUNC1((void))

								HX_STACK_LINE(111)
								tmp19->forEach( Dynamic(new _Function_6_1()),null());
							}
						}
						;break;
						case (int)0: {
							HX_STACK_LINE(117)
							::flixel::input::keyboard::FlxKeyboard tmp16 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp16,"tmp16");
							HX_STACK_LINE(117)
							bool tmp17 = tmp16->checkKeyArrayState(Array_obj< int >::__new().Add((int)37),(int)2);		HX_STACK_VAR(tmp17,"tmp17");
							HX_STACK_LINE(117)
							bool tmp18;		HX_STACK_VAR(tmp18,"tmp18");
							HX_STACK_LINE(117)
							if ((tmp17)){
								HX_STACK_LINE(117)
								Float tmp19 = (first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->x - (int)4);		HX_STACK_VAR(tmp19,"tmp19");
								HX_STACK_LINE(117)
								Float tmp20 = tmp19;		HX_STACK_VAR(tmp20,"tmp20");
								HX_STACK_LINE(117)
								Float tmp21 = tmp20;		HX_STACK_VAR(tmp21,"tmp21");
								HX_STACK_LINE(117)
								Float tmp22 = ::Math_obj::abs(tmp21);		HX_STACK_VAR(tmp22,"tmp22");
								HX_STACK_LINE(117)
								Float tmp23 = tmp22;		HX_STACK_VAR(tmp23,"tmp23");
								HX_STACK_LINE(117)
								tmp18 = (tmp23 < (int)20);
							}
							else{
								HX_STACK_LINE(117)
								tmp18 = false;
							}
							HX_STACK_LINE(117)
							if ((tmp18)){
								HX_STACK_LINE(118)
								correct = true;
								HX_STACK_LINE(119)
								::flixel::group::FlxTypedGroup tmp19 = this->dancers;		HX_STACK_VAR(tmp19,"tmp19");

								HX_BEGIN_LOCAL_FUNC_S0(hx::LocalFunc,_Function_6_1)
								int __ArgCount() const { return 1; }
								Void run(::flixel::FlxBasic obj){
									HX_STACK_FRAME("*","_Function_6_1",0x5204b87c,"*._Function_6_1","DDRState.hx",119,0x1943e03f)
									HX_STACK_ARG(obj,"obj")
									{
										HX_STACK_LINE(120)
										::flixel::FlxSprite dancer = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(dancer,"dancer");
										HX_STACK_LINE(121)
										dancer->animation->play(HX_HCSTRING("Left","\x27","\x34","\x89","\x32"),null(),null(),null());
									}
									return null();
								}
								HX_END_LOCAL_FUNC1((void))

								HX_STACK_LINE(119)
								tmp19->forEach( Dynamic(new _Function_6_1()),null());
							}
						}
						;break;
						case (int)2: {
							HX_STACK_LINE(125)
							::flixel::input::keyboard::FlxKeyboard tmp16 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp16,"tmp16");
							HX_STACK_LINE(125)
							bool tmp17 = tmp16->checkKeyArrayState(Array_obj< int >::__new().Add((int)39),(int)2);		HX_STACK_VAR(tmp17,"tmp17");
							HX_STACK_LINE(125)
							bool tmp18;		HX_STACK_VAR(tmp18,"tmp18");
							HX_STACK_LINE(125)
							if ((tmp17)){
								HX_STACK_LINE(125)
								Float tmp19 = (first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->x - (int)4);		HX_STACK_VAR(tmp19,"tmp19");
								HX_STACK_LINE(125)
								Float tmp20 = tmp19;		HX_STACK_VAR(tmp20,"tmp20");
								HX_STACK_LINE(125)
								Float tmp21 = tmp20;		HX_STACK_VAR(tmp21,"tmp21");
								HX_STACK_LINE(125)
								Float tmp22 = ::Math_obj::abs(tmp21);		HX_STACK_VAR(tmp22,"tmp22");
								HX_STACK_LINE(125)
								Float tmp23 = tmp22;		HX_STACK_VAR(tmp23,"tmp23");
								HX_STACK_LINE(125)
								tmp18 = (tmp23 < (int)20);
							}
							else{
								HX_STACK_LINE(125)
								tmp18 = false;
							}
							HX_STACK_LINE(125)
							if ((tmp18)){
								HX_STACK_LINE(126)
								correct = true;
								HX_STACK_LINE(127)
								::flixel::group::FlxTypedGroup tmp19 = this->dancers;		HX_STACK_VAR(tmp19,"tmp19");

								HX_BEGIN_LOCAL_FUNC_S0(hx::LocalFunc,_Function_6_1)
								int __ArgCount() const { return 1; }
								Void run(::flixel::FlxBasic obj){
									HX_STACK_FRAME("*","_Function_6_1",0x5204b87c,"*._Function_6_1","DDRState.hx",127,0x1943e03f)
									HX_STACK_ARG(obj,"obj")
									{
										HX_STACK_LINE(128)
										::flixel::FlxSprite dancer = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(dancer,"dancer");
										HX_STACK_LINE(129)
										dancer->animation->play(HX_HCSTRING("Right","\xbc","\x7b","\x91","\x7c"),null(),null(),null());
									}
									return null();
								}
								HX_END_LOCAL_FUNC1((void))

								HX_STACK_LINE(127)
								tmp19->forEach( Dynamic(new _Function_6_1()),null());
							}
						}
						;break;
						case (int)3: {
							HX_STACK_LINE(133)
							::flixel::input::keyboard::FlxKeyboard tmp16 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp16,"tmp16");
							HX_STACK_LINE(133)
							bool tmp17 = tmp16->checkKeyArrayState(Array_obj< int >::__new().Add((int)40),(int)2);		HX_STACK_VAR(tmp17,"tmp17");
							HX_STACK_LINE(133)
							bool tmp18;		HX_STACK_VAR(tmp18,"tmp18");
							HX_STACK_LINE(133)
							if ((tmp17)){
								HX_STACK_LINE(133)
								Float tmp19 = (first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->x - (int)4);		HX_STACK_VAR(tmp19,"tmp19");
								HX_STACK_LINE(133)
								Float tmp20 = tmp19;		HX_STACK_VAR(tmp20,"tmp20");
								HX_STACK_LINE(133)
								Float tmp21 = tmp20;		HX_STACK_VAR(tmp21,"tmp21");
								HX_STACK_LINE(133)
								Float tmp22 = ::Math_obj::abs(tmp21);		HX_STACK_VAR(tmp22,"tmp22");
								HX_STACK_LINE(133)
								Float tmp23 = tmp22;		HX_STACK_VAR(tmp23,"tmp23");
								HX_STACK_LINE(133)
								tmp18 = (tmp23 < (int)20);
							}
							else{
								HX_STACK_LINE(133)
								tmp18 = false;
							}
							HX_STACK_LINE(133)
							if ((tmp18)){
								HX_STACK_LINE(134)
								correct = true;
								HX_STACK_LINE(135)
								::flixel::group::FlxTypedGroup tmp19 = this->dancers;		HX_STACK_VAR(tmp19,"tmp19");

								HX_BEGIN_LOCAL_FUNC_S0(hx::LocalFunc,_Function_6_1)
								int __ArgCount() const { return 1; }
								Void run(::flixel::FlxBasic obj){
									HX_STACK_FRAME("*","_Function_6_1",0x5204b87c,"*._Function_6_1","DDRState.hx",135,0x1943e03f)
									HX_STACK_ARG(obj,"obj")
									{
										HX_STACK_LINE(136)
										::flixel::FlxSprite dancer = ((::flixel::FlxSprite)(obj));		HX_STACK_VAR(dancer,"dancer");
										HX_STACK_LINE(137)
										dancer->animation->play(HX_HCSTRING("Down","\x82","\x24","\x47","\x2d"),null(),null(),null());
									}
									return null();
								}
								HX_END_LOCAL_FUNC1((void))

								HX_STACK_LINE(135)
								tmp19->forEach( Dynamic(new _Function_6_1()),null());
							}
						}
						;break;
					}
				}
				HX_STACK_LINE(141)
				bool tmp14 = correct;		HX_STACK_VAR(tmp14,"tmp14");
				HX_STACK_LINE(141)
				if ((tmp14)){
					HX_STACK_LINE(142)
					first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->loadGraphic(HX_HCSTRING("assets/images/Level4/XCheckmark.png","\xac","\xd3","\xba","\xc7"),true,(int)40,(int)40,null(),null());
					HX_STACK_LINE(143)
					first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->animation->set_frameIndex((int)1);
					HX_STACK_LINE(144)
					::flixel::group::FlxTypedGroup tmp15 = this->prompts;		HX_STACK_VAR(tmp15,"tmp15");
					HX_STACK_LINE(144)
					::flixel::FlxSprite tmp16 = first->__get((int)0).StaticCast< ::flixel::FlxSprite >();		HX_STACK_VAR(tmp16,"tmp16");
					HX_STACK_LINE(144)
					tmp15->remove(tmp16,null());
					HX_STACK_LINE(145)
					::flixel::group::FlxTypedGroup tmp17 = this->outputs;		HX_STACK_VAR(tmp17,"tmp17");
					HX_STACK_LINE(145)
					::flixel::FlxSprite tmp18 = first->__get((int)0).StaticCast< ::flixel::FlxSprite >();		HX_STACK_VAR(tmp18,"tmp18");
					HX_STACK_LINE(145)
					tmp17->add(tmp18);
				}
				else{
					HX_STACK_LINE(148)
					first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->loadGraphic(HX_HCSTRING("assets/images/Level4/XCheckmark.png","\xac","\xd3","\xba","\xc7"),true,(int)40,(int)40,null(),null());
					HX_STACK_LINE(149)
					first->__get((int)0).StaticCast< ::flixel::FlxSprite >()->animation->set_frameIndex((int)0);
					HX_STACK_LINE(150)
					::flixel::group::FlxTypedGroup tmp15 = this->prompts;		HX_STACK_VAR(tmp15,"tmp15");
					HX_STACK_LINE(150)
					::flixel::FlxSprite tmp16 = first->__get((int)0).StaticCast< ::flixel::FlxSprite >();		HX_STACK_VAR(tmp16,"tmp16");
					HX_STACK_LINE(150)
					tmp15->remove(tmp16,null());
					HX_STACK_LINE(151)
					::flixel::group::FlxTypedGroup tmp17 = this->outputs;		HX_STACK_VAR(tmp17,"tmp17");
					HX_STACK_LINE(151)
					::flixel::FlxSprite tmp18 = first->__get((int)0).StaticCast< ::flixel::FlxSprite >();		HX_STACK_VAR(tmp18,"tmp18");
					HX_STACK_LINE(151)
					tmp17->add(tmp18);
					HX_STACK_LINE(152)
					(this->failure)++;
				}
			}
		}
		HX_STACK_LINE(157)
		int tmp8 = this->beatCounter;		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(157)
		int tmp9 = hx::Mod(tmp8,(int)24);		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(157)
		bool tmp10 = (tmp9 == (int)0);		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(157)
		if ((tmp10)){
			HX_STACK_LINE(158)
			::flixel::FlxSprite tmp11 = ::flixel::FlxSprite_obj::__new((int)320,(int)8,null());		HX_STACK_VAR(tmp11,"tmp11");
			HX_STACK_LINE(158)
			::flixel::FlxSprite arrow = tmp11;		HX_STACK_VAR(arrow,"arrow");
			HX_STACK_LINE(159)
			arrow->loadGraphic(HX_HCSTRING("assets/images/Level4/ArrowsSS.png","\x99","\xe1","\x0e","\x0b"),true,(int)40,(int)40,null(),null());
			HX_STACK_LINE(160)
			{
				HX_STACK_LINE(160)
				::String tmp12 = this->beats->shift();		HX_STACK_VAR(tmp12,"tmp12");
				HX_STACK_LINE(160)
				::String _g1 = tmp12;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(160)
				bool tmp13 = (_g1 != null());		HX_STACK_VAR(tmp13,"tmp13");
				HX_STACK_LINE(160)
				if ((tmp13)){
					HX_STACK_LINE(160)
					::String tmp14 = _g1;		HX_STACK_VAR(tmp14,"tmp14");
					HX_STACK_LINE(160)
					::String _switch_1 = (tmp14);
					if (  ( _switch_1==HX_HCSTRING("u","\x75","\x00","\x00","\x00"))){
						HX_STACK_LINE(162)
						arrow->animation->set_frameIndex((int)1);
						HX_STACK_LINE(163)
						::flixel::group::FlxTypedGroup tmp15 = this->prompts;		HX_STACK_VAR(tmp15,"tmp15");
						HX_STACK_LINE(163)
						::flixel::FlxSprite tmp16 = arrow;		HX_STACK_VAR(tmp16,"tmp16");
						HX_STACK_LINE(163)
						tmp15->add(tmp16);
					}
					else if (  ( _switch_1==HX_HCSTRING("l","\x6c","\x00","\x00","\x00"))){
						HX_STACK_LINE(165)
						arrow->animation->set_frameIndex((int)0);
						HX_STACK_LINE(166)
						::flixel::group::FlxTypedGroup tmp15 = this->prompts;		HX_STACK_VAR(tmp15,"tmp15");
						HX_STACK_LINE(166)
						::flixel::FlxSprite tmp16 = arrow;		HX_STACK_VAR(tmp16,"tmp16");
						HX_STACK_LINE(166)
						tmp15->add(tmp16);
					}
					else if (  ( _switch_1==HX_HCSTRING("r","\x72","\x00","\x00","\x00"))){
						HX_STACK_LINE(168)
						arrow->animation->set_frameIndex((int)2);
						HX_STACK_LINE(169)
						::flixel::group::FlxTypedGroup tmp15 = this->prompts;		HX_STACK_VAR(tmp15,"tmp15");
						HX_STACK_LINE(169)
						::flixel::FlxSprite tmp16 = arrow;		HX_STACK_VAR(tmp16,"tmp16");
						HX_STACK_LINE(169)
						tmp15->add(tmp16);
					}
					else if (  ( _switch_1==HX_HCSTRING("d","\x64","\x00","\x00","\x00"))){
						HX_STACK_LINE(171)
						arrow->animation->set_frameIndex((int)3);
						HX_STACK_LINE(172)
						::flixel::group::FlxTypedGroup tmp15 = this->prompts;		HX_STACK_VAR(tmp15,"tmp15");
						HX_STACK_LINE(172)
						::flixel::FlxSprite tmp16 = arrow;		HX_STACK_VAR(tmp16,"tmp16");
						HX_STACK_LINE(172)
						tmp15->add(tmp16);
					}
				}
			}
		}
		HX_STACK_LINE(175)
		Float tmp11 = elapsed;		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(175)
		this->super::update(tmp11);
	}
return null();
}



DDRState_obj::DDRState_obj()
{
}

void DDRState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(DDRState);
	HX_MARK_MEMBER_NAME(player,"player");
	HX_MARK_MEMBER_NAME(lincoln,"lincoln");
	HX_MARK_MEMBER_NAME(napoleon,"napoleon");
	HX_MARK_MEMBER_NAME(beatCounter,"beatCounter");
	HX_MARK_MEMBER_NAME(beats,"beats");
	HX_MARK_MEMBER_NAME(dancers,"dancers");
	HX_MARK_MEMBER_NAME(prompts,"prompts");
	HX_MARK_MEMBER_NAME(outputs,"outputs");
	HX_MARK_MEMBER_NAME(failure,"failure");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void DDRState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(player,"player");
	HX_VISIT_MEMBER_NAME(lincoln,"lincoln");
	HX_VISIT_MEMBER_NAME(napoleon,"napoleon");
	HX_VISIT_MEMBER_NAME(beatCounter,"beatCounter");
	HX_VISIT_MEMBER_NAME(beats,"beats");
	HX_VISIT_MEMBER_NAME(dancers,"dancers");
	HX_VISIT_MEMBER_NAME(prompts,"prompts");
	HX_VISIT_MEMBER_NAME(outputs,"outputs");
	HX_VISIT_MEMBER_NAME(failure,"failure");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic DDRState_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"beats") ) { return beats; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"lincoln") ) { return lincoln; }
		if (HX_FIELD_EQ(inName,"dancers") ) { return dancers; }
		if (HX_FIELD_EQ(inName,"prompts") ) { return prompts; }
		if (HX_FIELD_EQ(inName,"outputs") ) { return outputs; }
		if (HX_FIELD_EQ(inName,"failure") ) { return failure; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"napoleon") ) { return napoleon; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"beatCounter") ) { return beatCounter; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic DDRState_obj::__SetField(const ::String &inName,const Dynamic &inValue,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"beats") ) { beats=inValue.Cast< Array< ::String > >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"lincoln") ) { lincoln=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dancers") ) { dancers=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"prompts") ) { prompts=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"outputs") ) { outputs=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"failure") ) { failure=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"napoleon") ) { napoleon=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"beatCounter") ) { beatCounter=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void DDRState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"));
	outFields->push(HX_HCSTRING("lincoln","\xff","\x83","\x5f","\x1e"));
	outFields->push(HX_HCSTRING("napoleon","\xca","\xe5","\x71","\x4e"));
	outFields->push(HX_HCSTRING("beatCounter","\x26","\xb0","\x85","\xf5"));
	outFields->push(HX_HCSTRING("beats","\x3d","\x66","\x51","\xb0"));
	outFields->push(HX_HCSTRING("dancers","\xb4","\x59","\xd1","\x9e"));
	outFields->push(HX_HCSTRING("prompts","\x4f","\x1e","\xef","\xf7"));
	outFields->push(HX_HCSTRING("outputs","\x52","\x12","\x6c","\xe4"));
	outFields->push(HX_HCSTRING("failure","\x4a","\x70","\xfa","\x16"));
	super::__GetFields(outFields);
};

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(DDRState_obj,player),HX_HCSTRING("player","\x61","\xeb","\xb8","\x37")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(DDRState_obj,lincoln),HX_HCSTRING("lincoln","\xff","\x83","\x5f","\x1e")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(DDRState_obj,napoleon),HX_HCSTRING("napoleon","\xca","\xe5","\x71","\x4e")},
	{hx::fsInt,(int)offsetof(DDRState_obj,beatCounter),HX_HCSTRING("beatCounter","\x26","\xb0","\x85","\xf5")},
	{hx::fsObject /*Array< ::String >*/ ,(int)offsetof(DDRState_obj,beats),HX_HCSTRING("beats","\x3d","\x66","\x51","\xb0")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(DDRState_obj,dancers),HX_HCSTRING("dancers","\xb4","\x59","\xd1","\x9e")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(DDRState_obj,prompts),HX_HCSTRING("prompts","\x4f","\x1e","\xef","\xf7")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(DDRState_obj,outputs),HX_HCSTRING("outputs","\x52","\x12","\x6c","\xe4")},
	{hx::fsInt,(int)offsetof(DDRState_obj,failure),HX_HCSTRING("failure","\x4a","\x70","\xfa","\x16")},
	{ hx::fsUnknown, 0, null()}
};
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"),
	HX_HCSTRING("lincoln","\xff","\x83","\x5f","\x1e"),
	HX_HCSTRING("napoleon","\xca","\xe5","\x71","\x4e"),
	HX_HCSTRING("beatCounter","\x26","\xb0","\x85","\xf5"),
	HX_HCSTRING("beats","\x3d","\x66","\x51","\xb0"),
	HX_HCSTRING("dancers","\xb4","\x59","\xd1","\x9e"),
	HX_HCSTRING("prompts","\x4f","\x1e","\xef","\xf7"),
	HX_HCSTRING("outputs","\x52","\x12","\x6c","\xe4"),
	HX_HCSTRING("failure","\x4a","\x70","\xfa","\x16"),
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(DDRState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(DDRState_obj::__mClass,"__mClass");
};

#endif

hx::Class DDRState_obj::__mClass;

void DDRState_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("DDRState","\xdf","\x57","\x96","\x1d");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< DDRState_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

