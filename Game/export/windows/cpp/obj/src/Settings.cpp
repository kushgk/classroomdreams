#include <hxcpp.h>

#ifndef INCLUDED_Settings
#include <Settings.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif

Void Settings_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("Settings","new",0xb86f1775,"Settings.new","Settings.hx",10,0xc9b94d9b)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(10)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(10)
	super::__construct(tmp);
}
;
	return null();
}

//Settings_obj::~Settings_obj() { }

Dynamic Settings_obj::__CreateEmpty() { return  new Settings_obj; }
hx::ObjectPtr< Settings_obj > Settings_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< Settings_obj > _result_ = new Settings_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic Settings_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Settings_obj > _result_ = new Settings_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void Settings_obj::create( ){
{
		HX_STACK_FRAME("Settings","create",0x82dbfb07,"Settings.create","Settings.hx",12,0xc9b94d9b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(12)
		this->super::create();
	}
return null();
}


Void Settings_obj::update( Float elapsed){
{
		HX_STACK_FRAME("Settings","update",0x8dd21a14,"Settings.update","Settings.hx",15,0xc9b94d9b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(16)
		Float tmp = elapsed;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(16)
		this->super::update(tmp);
	}
return null();
}



Settings_obj::Settings_obj()
{
}

Dynamic Settings_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Settings_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Settings_obj::__mClass,"__mClass");
};

#endif

hx::Class Settings_obj::__mClass;

void Settings_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("Settings","\x03","\x39","\xef","\x64");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< Settings_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

