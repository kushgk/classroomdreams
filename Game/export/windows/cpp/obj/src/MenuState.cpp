#include <hxcpp.h>

#ifndef INCLUDED_BoxerState
#include <BoxerState.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInput
#include <flixel/input/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSoundGroup
#include <flixel/system/FlxSoundGroup.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton_flixel_text_FlxText
#include <flixel/ui/FlxTypedButton_flixel_text_FlxText.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void MenuState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("MenuState","new",0xe563b1c4,"MenuState.new","MenuState.hx",10,0xdfbcb22c)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(10)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(10)
	super::__construct(tmp);
}
;
	return null();
}

//MenuState_obj::~MenuState_obj() { }

Dynamic MenuState_obj::__CreateEmpty() { return  new MenuState_obj; }
hx::ObjectPtr< MenuState_obj > MenuState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< MenuState_obj > _result_ = new MenuState_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic MenuState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MenuState_obj > _result_ = new MenuState_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void MenuState_obj::create( ){
{
		HX_STACK_FRAME("MenuState","create",0xe57b7c18,"MenuState.create","MenuState.hx",13,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(14)
		this->super::create();
		HX_STACK_LINE(15)
		::flixel::FlxSprite tmp = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/MainMenu/menubg.png","\xeb","\x30","\x7c","\xf9"));		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(15)
		::flixel::FlxSprite bg = tmp;		HX_STACK_VAR(bg,"bg");
		HX_STACK_LINE(16)
		::flixel::FlxSprite tmp1 = bg;		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(16)
		this->add(tmp1);
		HX_STACK_LINE(18)
		::flixel::FlxSprite tmp2 = ::flixel::FlxSprite_obj::__new((int)80,(int)25,HX_HCSTRING("assets/images/MainMenu/title.png","\xc7","\x23","\x6d","\x2a"));		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(18)
		::flixel::FlxSprite title = tmp2;		HX_STACK_VAR(title,"title");
		HX_STACK_LINE(19)
		::flixel::FlxSprite tmp3 = title;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(19)
		this->add(tmp3);
		HX_STACK_LINE(21)
		Dynamic tmp4 = this->play_dyn();		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(21)
		::flixel::ui::FlxButton tmp5 = ::flixel::ui::FlxButton_obj::__new((int)130,(int)138,HX_HCSTRING("","\x00","\x00","\x00","\x00"),tmp4);		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(21)
		::flixel::ui::FlxButton playButt = tmp5;		HX_STACK_VAR(playButt,"playButt");
		HX_STACK_LINE(22)
		playButt->loadGraphic(HX_HCSTRING("assets/images/MainMenu/playbutton.png","\xad","\x6d","\xc6","\x2f"),true,(int)66,(int)22,null(),null());
		HX_STACK_LINE(23)
		::flixel::ui::FlxButton tmp6 = playButt;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(23)
		this->add(tmp6);
		HX_STACK_LINE(24)
		Dynamic tmp7 = this->settings_dyn();		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(24)
		::flixel::ui::FlxButton tmp8 = ::flixel::ui::FlxButton_obj::__new((int)90,(int)167,HX_HCSTRING("","\x00","\x00","\x00","\x00"),tmp7);		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(24)
		::flixel::ui::FlxButton setButt = tmp8;		HX_STACK_VAR(setButt,"setButt");
		HX_STACK_LINE(25)
		setButt->loadGraphic(HX_HCSTRING("assets/images/MainMenu/settingsbutt.png","\x3d","\xf2","\x8b","\xe4"),true,(int)156,(int)22,null(),null());
		HX_STACK_LINE(26)
		::flixel::ui::FlxButton tmp9 = setButt;		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(26)
		this->add(tmp9);
		HX_STACK_LINE(28)
		::flixel::FlxSprite tmp10 = ::flixel::FlxSprite_obj::__new((int)100,(int)222,HX_HCSTRING("assets/images/MainMenu/insertcoin.png","\x11","\x7a","\xec","\x25"));		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(28)
		::flixel::FlxSprite insertCoin = tmp10;		HX_STACK_VAR(insertCoin,"insertCoin");
		HX_STACK_LINE(29)
		::flixel::FlxSprite tmp11 = insertCoin;		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(29)
		this->add(tmp11);
		HX_STACK_LINE(31)
		::flixel::_system::frontEnds::SoundFrontEnd tmp12 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(31)
		tmp12->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/hyper-partial4.wav","\x48","\x23","\xda","\xcc"),(int)1,true,null());
	}
return null();
}


Void MenuState_obj::update( Float elapsed){
{
		HX_STACK_FRAME("MenuState","update",0xf0719b25,"MenuState.update","MenuState.hx",35,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(36)
		::flixel::_system::frontEnds::SoundFrontEnd tmp = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(36)
		::flixel::_system::FlxSound tmp1 = tmp->__Field(HX_HCSTRING("music","\xa5","\xd0","\x5a","\x10"), hx::paccDynamic );		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(36)
		bool tmp2 = (tmp1 == null());		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(36)
		if ((tmp2)){
			HX_STACK_LINE(38)
			::flixel::_system::frontEnds::SoundFrontEnd tmp3 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(38)
			tmp3->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/hyper-partial4.wav","\x48","\x23","\xda","\xcc"),(int)1,true,null());
		}
		HX_STACK_LINE(40)
		Float tmp3 = elapsed;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(40)
		this->super::update(tmp3);
	}
return null();
}


Void MenuState_obj::play( ){
{
		HX_STACK_FRAME("MenuState","play",0xd32f8410,"MenuState.play","MenuState.hx",44,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(44)
		::BoxerState tmp = ::BoxerState_obj::__new(null());		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(44)
		::flixel::FlxState nextState = tmp;		HX_STACK_VAR(nextState,"nextState");
		HX_STACK_LINE(44)
		::flixel::FlxGame tmp1 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(44)
		::flixel::FlxState tmp2 = nextState;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(44)
		bool tmp3 = tmp1->_state->switchTo(tmp2);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(44)
		if ((tmp3)){
			HX_STACK_LINE(44)
			::flixel::FlxGame tmp4 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(44)
			tmp4->_requestedState = nextState;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,play,(void))

Void MenuState_obj::settings( ){
{
		HX_STACK_FRAME("MenuState","settings",0x37ae10ff,"MenuState.settings","MenuState.hx",47,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(48)
		bool tmp = ::flixel::FlxG_obj::get_fullscreen();		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(48)
		bool tmp1 = !(tmp);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(48)
		::flixel::FlxG_obj::set_fullscreen(tmp1);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,settings,(void))


MenuState_obj::MenuState_obj()
{
}

Dynamic MenuState_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"play") ) { return play_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"settings") ) { return settings_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	HX_HCSTRING("play","\xf4","\x2d","\x5a","\x4a"),
	HX_HCSTRING("settings","\xe3","\x7c","\x3d","\x8b"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
};

#endif

hx::Class MenuState_obj::__mClass;

void MenuState_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("MenuState","\xd2","\xbf","\xb6","\xc0");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< MenuState_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

