#include <hxcpp.h>

#ifndef INCLUDED_Book
#include <Book.h>
#endif
#ifndef INCLUDED_BoxerState
#include <BoxerState.h>
#endif
#ifndef INCLUDED_PlayerBoxer
#include <PlayerBoxer.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSoundGroup
#include <flixel/system/FlxSoundGroup.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif

Void BoxerState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("BoxerState","new",0x7227d14b,"BoxerState.new","BoxerState.hx",10,0xebd92085)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(10)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(10)
	super::__construct(tmp);
}
;
	return null();
}

//BoxerState_obj::~BoxerState_obj() { }

Dynamic BoxerState_obj::__CreateEmpty() { return  new BoxerState_obj; }
hx::ObjectPtr< BoxerState_obj > BoxerState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< BoxerState_obj > _result_ = new BoxerState_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic BoxerState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< BoxerState_obj > _result_ = new BoxerState_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void BoxerState_obj::create( ){
{
		HX_STACK_FRAME("BoxerState","create",0x82108af1,"BoxerState.create","BoxerState.hx",13,0xebd92085)
		HX_STACK_THIS(this)
		HX_STACK_LINE(14)
		this->super::create();
		HX_STACK_LINE(15)
		::flixel::FlxSprite tmp = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/Level1/Stage.png","\x02","\x33","\x0a","\xa3"));		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(15)
		this->add(tmp);
		HX_STACK_LINE(16)
		::PlayerBoxer tmp1 = ::PlayerBoxer_obj::__new();		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(16)
		::PlayerBoxer player = tmp1;		HX_STACK_VAR(player,"player");
		HX_STACK_LINE(17)
		::Book tmp2 = ::Book_obj::__new(player);		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(17)
		this->add(tmp2);
		HX_STACK_LINE(18)
		::PlayerBoxer tmp3 = player;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(18)
		this->add(tmp3);
		HX_STACK_LINE(19)
		::flixel::FlxSprite tmp4 = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/Level1/StageOverlay.png","\xc4","\x61","\x55","\xf8"));		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(19)
		this->add(tmp4);
		HX_STACK_LINE(20)
		::flixel::_system::frontEnds::SoundFrontEnd tmp5 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(20)
		tmp5->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/drowsy2.wav","\x9e","\xa3","\x51","\xc6"),(int)1,true,null());
	}
return null();
}


Void BoxerState_obj::update( Float elapsed){
{
		HX_STACK_FRAME("BoxerState","update",0x8d06a9fe,"BoxerState.update","BoxerState.hx",24,0xebd92085)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(25)
		::flixel::_system::frontEnds::SoundFrontEnd tmp = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(25)
		::flixel::_system::FlxSound tmp1 = tmp->__Field(HX_HCSTRING("music","\xa5","\xd0","\x5a","\x10"), hx::paccDynamic );		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(25)
		bool tmp2 = (tmp1 == null());		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(25)
		if ((tmp2)){
			HX_STACK_LINE(27)
			::flixel::_system::frontEnds::SoundFrontEnd tmp3 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(27)
			tmp3->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/drowssy2.wav","\xe3","\xba","\x5d","\x48"),(int)1,true,null());
		}
		HX_STACK_LINE(29)
		Float tmp3 = elapsed;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(29)
		this->super::update(tmp3);
	}
return null();
}



BoxerState_obj::BoxerState_obj()
{
}

Dynamic BoxerState_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(BoxerState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(BoxerState_obj::__mClass,"__mClass");
};

#endif

hx::Class BoxerState_obj::__mClass;

void BoxerState_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("BoxerState","\xd9","\xcf","\x64","\xed");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< BoxerState_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

