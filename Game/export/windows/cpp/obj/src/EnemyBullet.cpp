#include <hxcpp.h>

#ifndef INCLUDED_EnemyBullet
#include <EnemyBullet.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_math_FlxPoint
#include <flixel/math/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxPooled
#include <flixel/util/IFlxPooled.h>
#endif

Void EnemyBullet_obj::__construct(Dynamic x,Dynamic y)
{
HX_STACK_FRAME("EnemyBullet","new",0x2e00447c,"EnemyBullet.new","Enemybullet.hx",9,0x3633fe94)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
{
	HX_STACK_LINE(10)
	Dynamic tmp = x;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(10)
	Dynamic tmp1 = y;		HX_STACK_VAR(tmp1,"tmp1");
	HX_STACK_LINE(10)
	super::__construct(tmp,tmp1,HX_HCSTRING("assets/images/Level2/Enemybullet.png","\x8f","\x27","\xff","\xf6"));
	HX_STACK_LINE(11)
	::flixel::math::FlxPoint tmp2 = this->velocity;		HX_STACK_VAR(tmp2,"tmp2");
	HX_STACK_LINE(11)
	tmp2->set_y((int)180);
}
;
	return null();
}

//EnemyBullet_obj::~EnemyBullet_obj() { }

Dynamic EnemyBullet_obj::__CreateEmpty() { return  new EnemyBullet_obj; }
hx::ObjectPtr< EnemyBullet_obj > EnemyBullet_obj::__new(Dynamic x,Dynamic y)
{  hx::ObjectPtr< EnemyBullet_obj > _result_ = new EnemyBullet_obj();
	_result_->__construct(x,y);
	return _result_;}

Dynamic EnemyBullet_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< EnemyBullet_obj > _result_ = new EnemyBullet_obj();
	_result_->__construct(inArgs[0],inArgs[1]);
	return _result_;}


EnemyBullet_obj::EnemyBullet_obj()
{
}

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(EnemyBullet_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(EnemyBullet_obj::__mClass,"__mClass");
};

#endif

hx::Class EnemyBullet_obj::__mClass;

void EnemyBullet_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("EnemyBullet","\x8a","\x96","\x75","\xa0");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(0 /* sMemberFields */);
	__mClass->mCanCast = hx::TCanCast< EnemyBullet_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

