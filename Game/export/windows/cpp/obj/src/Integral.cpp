#include <hxcpp.h>

#include "hxMath.h"
#ifndef INCLUDED_Integral
#include <Integral.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_math_FlxRandom
#include <flixel/math/FlxRandom.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void Integral_obj::__construct(Dynamic x,Dynamic y)
{
HX_STACK_FRAME("Integral","new",0x18a02a7e,"Integral.new","Integral.hx",20,0xf6ed2eb2)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
{
	HX_STACK_LINE(21)
	Dynamic tmp = x;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(21)
	Dynamic tmp1 = y;		HX_STACK_VAR(tmp1,"tmp1");
	HX_STACK_LINE(21)
	super::__construct(tmp,tmp1,null());
	HX_STACK_LINE(22)
	this->loadGraphic(HX_HCSTRING("assets/images/Level2/IntegrateSS.png","\xd6","\x01","\x39","\x41"),true,null(),null(),null(),null());
	HX_STACK_LINE(25)
	::flixel::animation::FlxAnimationController tmp2 = this->animation;		HX_STACK_VAR(tmp2,"tmp2");
	HX_STACK_LINE(25)
	::flixel::math::FlxRandom tmp3 = ::flixel::FlxG_obj::random;		HX_STACK_VAR(tmp3,"tmp3");
	HX_STACK_LINE(25)
	Float tmp4 = tmp3->_float(null(),null(),null());		HX_STACK_VAR(tmp4,"tmp4");
	HX_STACK_LINE(25)
	Float tmp5 = (tmp4 * (int)4);		HX_STACK_VAR(tmp5,"tmp5");
	HX_STACK_LINE(25)
	Float tmp6 = ((int)6 + tmp5);		HX_STACK_VAR(tmp6,"tmp6");
	HX_STACK_LINE(25)
	int tmp7 = ::Math_obj::floor(tmp6);		HX_STACK_VAR(tmp7,"tmp7");
	HX_STACK_LINE(25)
	tmp2->add(HX_HCSTRING("Default","\xa1","\x00","\x15","\x69"),Array_obj< int >::__new().Add((int)0),tmp7,null(),null(),null());
	HX_STACK_LINE(28)
	::flixel::animation::FlxAnimationController tmp8 = this->animation;		HX_STACK_VAR(tmp8,"tmp8");
	HX_STACK_LINE(28)
	tmp8->play(HX_HCSTRING("Default","\xa1","\x00","\x15","\x69"),null(),null(),null());
	HX_STACK_LINE(30)
	this->stepCount = (int)-59;
	HX_STACK_LINE(31)
	this->moving = true;
	HX_STACK_LINE(32)
	this->dir = (int)1;
}
;
	return null();
}

//Integral_obj::~Integral_obj() { }

Dynamic Integral_obj::__CreateEmpty() { return  new Integral_obj; }
hx::ObjectPtr< Integral_obj > Integral_obj::__new(Dynamic x,Dynamic y)
{  hx::ObjectPtr< Integral_obj > _result_ = new Integral_obj();
	_result_->__construct(x,y);
	return _result_;}

Dynamic Integral_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Integral_obj > _result_ = new Integral_obj();
	_result_->__construct(inArgs[0],inArgs[1]);
	return _result_;}

Void Integral_obj::update( Float elapsed){
{
		HX_STACK_FRAME("Integral","update",0x3741d2ab,"Integral.update","Integral.hx",35,0xf6ed2eb2)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(36)
		bool tmp = this->moving;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(36)
		if ((tmp)){
			HX_STACK_LINE(37)
			int tmp1 = this->stepCount;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(37)
			bool tmp2 = (tmp1 < (int)120);		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(37)
			if ((tmp2)){
				HX_STACK_LINE(38)
				int tmp3 = this->stepCount;		HX_STACK_VAR(tmp3,"tmp3");
				HX_STACK_LINE(38)
				int tmp4 = hx::Mod(tmp3,(int)60);		HX_STACK_VAR(tmp4,"tmp4");
				HX_STACK_LINE(38)
				bool tmp5 = (tmp4 == (int)0);		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(38)
				if ((tmp5)){
					HX_STACK_LINE(39)
					::Integral _g = hx::ObjectPtr<OBJ_>(this);		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(39)
					Float tmp6 = _g->x;		HX_STACK_VAR(tmp6,"tmp6");
					HX_STACK_LINE(39)
					int tmp7 = this->dir;		HX_STACK_VAR(tmp7,"tmp7");
					HX_STACK_LINE(39)
					int tmp8 = (tmp7 * (int)32);		HX_STACK_VAR(tmp8,"tmp8");
					HX_STACK_LINE(39)
					Float tmp9 = (tmp6 + tmp8);		HX_STACK_VAR(tmp9,"tmp9");
					HX_STACK_LINE(39)
					_g->set_x(tmp9);
				}
				HX_STACK_LINE(41)
				(this->stepCount)++;
			}
			else{
				HX_STACK_LINE(43)
				int tmp3 = this->stepCount;		HX_STACK_VAR(tmp3,"tmp3");
				HX_STACK_LINE(43)
				int tmp4 = hx::Mod(tmp3,(int)60);		HX_STACK_VAR(tmp4,"tmp4");
				HX_STACK_LINE(43)
				bool tmp5 = (tmp4 == (int)0);		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(43)
				if ((tmp5)){
					HX_STACK_LINE(44)
					{
						HX_STACK_LINE(44)
						::Integral _g = hx::ObjectPtr<OBJ_>(this);		HX_STACK_VAR(_g,"_g");
						HX_STACK_LINE(44)
						Float tmp6 = (_g->y + (int)32);		HX_STACK_VAR(tmp6,"tmp6");
						HX_STACK_LINE(44)
						_g->set_y(tmp6);
					}
					HX_STACK_LINE(45)
					this->stepCount = (int)-59;
					HX_STACK_LINE(46)
					hx::MultEq(this->dir,(int)-1);
				}
			}
		}
		HX_STACK_LINE(51)
		Float tmp1 = this->y;		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(51)
		int tmp2 = (int)176;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(51)
		bool tmp3 = (tmp1 > tmp2);		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(51)
		if ((tmp3)){
			HX_STACK_LINE(52)
			::MenuState tmp4 = ::MenuState_obj::__new(null());		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(52)
			::flixel::FlxState nextState = tmp4;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(52)
			::flixel::FlxGame tmp5 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp5,"tmp5");
			HX_STACK_LINE(52)
			::flixel::FlxState tmp6 = nextState;		HX_STACK_VAR(tmp6,"tmp6");
			HX_STACK_LINE(52)
			bool tmp7 = tmp5->_state->switchTo(tmp6);		HX_STACK_VAR(tmp7,"tmp7");
			HX_STACK_LINE(52)
			if ((tmp7)){
				HX_STACK_LINE(52)
				::flixel::FlxGame tmp8 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp8,"tmp8");
				HX_STACK_LINE(52)
				tmp8->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(55)
		Float tmp4 = elapsed;		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(55)
		this->super::update(tmp4);
	}
return null();
}


Void Integral_obj::die( ){
{
		HX_STACK_FRAME("Integral","die",0x1898975e,"Integral.die","Integral.hx",58,0xf6ed2eb2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(59)
		this->moving = false;
		HX_STACK_LINE(60)
		this->loadGraphic(HX_HCSTRING("assets/images/Level2/PuffSS.png","\x96","\x0f","\xe3","\xbd"),true,(int)48,(int)48,null(),null());
		HX_STACK_LINE(61)
		::flixel::animation::FlxAnimationController tmp = this->animation;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(61)
		tmp->add(HX_HCSTRING("blow up","\x49","\xe4","\xfd","\x51"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4),(int)8,false,null(),null());
		HX_STACK_LINE(62)
		Dynamic tmp1 = this->suicide_dyn();		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(62)
		::flixel::animation::FlxAnimationController tmp2 = this->animation;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(62)
		tmp2->finishCallback = tmp1;
		HX_STACK_LINE(63)
		::flixel::animation::FlxAnimationController tmp3 = this->animation;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(63)
		tmp3->play(HX_HCSTRING("blow up","\x49","\xe4","\xfd","\x51"),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Integral_obj,die,(void))

Void Integral_obj::suicide( ::String fummy){
{
		HX_STACK_FRAME("Integral","suicide",0xb13f5dcc,"Integral.suicide","Integral.hx",67,0xf6ed2eb2)
		HX_STACK_THIS(this)
		HX_STACK_ARG(fummy,"fummy")
		HX_STACK_LINE(67)
		this->destroy();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Integral_obj,suicide,(void))


Integral_obj::Integral_obj()
{
}

Dynamic Integral_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"dir") ) { return dir; }
		if (HX_FIELD_EQ(inName,"die") ) { return die_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"moving") ) { return moving; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"suicide") ) { return suicide_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"stepCount") ) { return stepCount; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_shotClock") ) { return _shotClock; }
		if (HX_FIELD_EQ(inName,"_originalX") ) { return _originalX; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Integral_obj::__SetField(const ::String &inName,const Dynamic &inValue,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"dir") ) { dir=inValue.Cast< int >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"moving") ) { moving=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"stepCount") ) { stepCount=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_shotClock") ) { _shotClock=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_originalX") ) { _originalX=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Integral_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_HCSTRING("_shotClock","\x35","\xd6","\x57","\x9f"));
	outFields->push(HX_HCSTRING("_originalX","\xa8","\x0e","\xe8","\x45"));
	outFields->push(HX_HCSTRING("stepCount","\xe3","\xd9","\xce","\xba"));
	outFields->push(HX_HCSTRING("dir","\x4d","\x3d","\x4c","\x00"));
	outFields->push(HX_HCSTRING("moving","\x8e","\xf2","\xaf","\xcc"));
	super::__GetFields(outFields);
};

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsFloat,(int)offsetof(Integral_obj,_shotClock),HX_HCSTRING("_shotClock","\x35","\xd6","\x57","\x9f")},
	{hx::fsInt,(int)offsetof(Integral_obj,_originalX),HX_HCSTRING("_originalX","\xa8","\x0e","\xe8","\x45")},
	{hx::fsInt,(int)offsetof(Integral_obj,stepCount),HX_HCSTRING("stepCount","\xe3","\xd9","\xce","\xba")},
	{hx::fsInt,(int)offsetof(Integral_obj,dir),HX_HCSTRING("dir","\x4d","\x3d","\x4c","\x00")},
	{hx::fsBool,(int)offsetof(Integral_obj,moving),HX_HCSTRING("moving","\x8e","\xf2","\xaf","\xcc")},
	{ hx::fsUnknown, 0, null()}
};
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("_shotClock","\x35","\xd6","\x57","\x9f"),
	HX_HCSTRING("_originalX","\xa8","\x0e","\xe8","\x45"),
	HX_HCSTRING("stepCount","\xe3","\xd9","\xce","\xba"),
	HX_HCSTRING("dir","\x4d","\x3d","\x4c","\x00"),
	HX_HCSTRING("moving","\x8e","\xf2","\xaf","\xcc"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	HX_HCSTRING("die","\x40","\x3d","\x4c","\x00"),
	HX_HCSTRING("suicide","\xae","\x9c","\xb8","\x2c"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Integral_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Integral_obj::__mClass,"__mClass");
};

#endif

hx::Class Integral_obj::__mClass;

void Integral_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("Integral","\x8c","\xd3","\xe5","\xfa");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< Integral_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

