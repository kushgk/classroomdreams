#include <hxcpp.h>

#include "hxMath.h"
#ifndef INCLUDED_Book
#include <Book.h>
#endif
#ifndef INCLUDED_PlayerBoxer
#include <PlayerBoxer.h>
#endif
#ifndef INCLUDED_SpaceState
#include <SpaceState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void Book_obj::__construct(::PlayerBoxer play)
{
HX_STACK_FRAME("Book","new",0xe308a9bb,"Book.new","Book.hx",14,0x4a7d2615)
HX_STACK_THIS(this)
HX_STACK_ARG(play,"play")
{
	HX_STACK_LINE(15)
	super::__construct((int)120,(int)20,null());
	HX_STACK_LINE(16)
	this->loadGraphic(HX_HCSTRING("assets/images/Level1/BookSheet.png","\x7a","\xf6","\xff","\x01"),true,(int)100,(int)112,null(),null());
	HX_STACK_LINE(17)
	::flixel::animation::FlxAnimationController tmp = this->animation;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(17)
	tmp->add(HX_HCSTRING("idle","\x14","\xa7","\xb3","\x45"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)5,false,null(),null());
	HX_STACK_LINE(18)
	::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
	HX_STACK_LINE(18)
	tmp1->add(HX_HCSTRING("leftPunch","\x27","\xac","\x6a","\xdf"),Array_obj< int >::__new().Add((int)5).Add((int)6).Add((int)7).Add((int)8).Add((int)9),(int)5,false,null(),null());
	HX_STACK_LINE(19)
	::flixel::animation::FlxAnimationController tmp2 = this->animation;		HX_STACK_VAR(tmp2,"tmp2");
	HX_STACK_LINE(19)
	tmp2->add(HX_HCSTRING("rightPunch","\x32","\xa4","\x4f","\xcd"),Array_obj< int >::__new().Add((int)10).Add((int)11).Add((int)12).Add((int)13).Add((int)14),(int)5,false,null(),null());
	HX_STACK_LINE(20)
	::flixel::animation::FlxAnimationController tmp3 = this->animation;		HX_STACK_VAR(tmp3,"tmp3");
	HX_STACK_LINE(20)
	tmp3->add(HX_HCSTRING("knock","\x14","\x1b","\xea","\xe4"),Array_obj< int >::__new().Add((int)15).Add((int)16).Add((int)17).Add((int)18),(int)3,false,null(),null());
	HX_STACK_LINE(21)
	Dynamic tmp4 = this->stateChange_dyn();		HX_STACK_VAR(tmp4,"tmp4");
	HX_STACK_LINE(21)
	::flixel::animation::FlxAnimationController tmp5 = this->animation;		HX_STACK_VAR(tmp5,"tmp5");
	HX_STACK_LINE(21)
	tmp5->finishCallback = tmp4;
	HX_STACK_LINE(23)
	this->health = (int)200;
	HX_STACK_LINE(24)
	this->player = play;
	HX_STACK_LINE(25)
	::PlayerBoxer tmp6 = this->player;		HX_STACK_VAR(tmp6,"tmp6");
	HX_STACK_LINE(25)
	tmp6->book = hx::ObjectPtr<OBJ_>(this);
	HX_STACK_LINE(26)
	::flixel::animation::FlxAnimationController tmp7 = this->animation;		HX_STACK_VAR(tmp7,"tmp7");
	HX_STACK_LINE(26)
	tmp7->play(HX_HCSTRING("idle","\x14","\xa7","\xb3","\x45"),null(),null(),null());
}
;
	return null();
}

//Book_obj::~Book_obj() { }

Dynamic Book_obj::__CreateEmpty() { return  new Book_obj; }
hx::ObjectPtr< Book_obj > Book_obj::__new(::PlayerBoxer play)
{  hx::ObjectPtr< Book_obj > _result_ = new Book_obj();
	_result_->__construct(play);
	return _result_;}

Dynamic Book_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Book_obj > _result_ = new Book_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void Book_obj::stateChange( ::String anim){
{
		HX_STACK_FRAME("Book","stateChange",0x86820abc,"Book.stateChange","Book.hx",29,0x4a7d2615)
		HX_STACK_THIS(this)
		HX_STACK_ARG(anim,"anim")
		HX_STACK_LINE(30)
		::String tmp = anim;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(30)
		::String _switch_1 = (tmp);
		if (  ( _switch_1==HX_HCSTRING("leftPunch","\x27","\xac","\x6a","\xdf"))){
			HX_STACK_LINE(32)
			::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(32)
			tmp1->play(HX_HCSTRING("idle","\x14","\xa7","\xb3","\x45"),null(),null(),null());
			HX_STACK_LINE(33)
			::PlayerBoxer tmp2 = this->player;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(33)
			::String tmp3 = tmp2->animation->get_name();		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(33)
			bool tmp4 = (tmp3 != HX_HCSTRING("RigDod","\xa9","\x73","\x9f","\x82"));		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(33)
			if ((tmp4)){
				HX_STACK_LINE(34)
				::PlayerBoxer tmp5 = this->player;		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(34)
				hx::SubEq(tmp5->health,(int)34);
			}
		}
		else if (  ( _switch_1==HX_HCSTRING("rightPunch","\x32","\xa4","\x4f","\xcd"))){
			HX_STACK_LINE(37)
			::flixel::animation::FlxAnimationController tmp1 = this->animation;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(37)
			tmp1->play(HX_HCSTRING("idle","\x14","\xa7","\xb3","\x45"),null(),null(),null());
			HX_STACK_LINE(38)
			::PlayerBoxer tmp2 = this->player;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(38)
			::String tmp3 = tmp2->animation->get_name();		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(38)
			bool tmp4 = (tmp3 != HX_HCSTRING("LefDod","\xcc","\xc0","\x37","\xce"));		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(38)
			if ((tmp4)){
				HX_STACK_LINE(39)
				::PlayerBoxer tmp5 = this->player;		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(39)
				hx::SubEq(tmp5->health,(int)34);
			}
		}
		else if (  ( _switch_1==HX_HCSTRING("idle","\x14","\xa7","\xb3","\x45"))){
			HX_STACK_LINE(42)
			Float tmp1 = ::Math_obj::random();		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(42)
			int tmp2 = ::Math_obj::round(tmp1);		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(42)
			bool tmp3 = (tmp2 == (int)0);		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(42)
			if ((tmp3)){
				HX_STACK_LINE(43)
				::flixel::animation::FlxAnimationController tmp4 = this->animation;		HX_STACK_VAR(tmp4,"tmp4");
				HX_STACK_LINE(43)
				tmp4->play(HX_HCSTRING("leftPunch","\x27","\xac","\x6a","\xdf"),null(),null(),null());
			}
			else{
				HX_STACK_LINE(45)
				::flixel::animation::FlxAnimationController tmp4 = this->animation;		HX_STACK_VAR(tmp4,"tmp4");
				HX_STACK_LINE(45)
				tmp4->play(HX_HCSTRING("rightPunch","\x32","\xa4","\x4f","\xcd"),null(),null(),null());
			}
		}
		else if (  ( _switch_1==HX_HCSTRING("knock","\x14","\x1b","\xea","\xe4"))){
			HX_STACK_LINE(48)
			::SpaceState tmp1 = ::SpaceState_obj::__new(null());		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(48)
			::flixel::FlxState nextState = tmp1;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(48)
			::flixel::FlxGame tmp2 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(48)
			::flixel::FlxState tmp3 = nextState;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(48)
			bool tmp4 = tmp2->_state->switchTo(tmp3);		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(48)
			if ((tmp4)){
				HX_STACK_LINE(48)
				::flixel::FlxGame tmp5 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(48)
				tmp5->_requestedState = nextState;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Book_obj,stateChange,(void))

Void Book_obj::update( Float elapsed){
{
		HX_STACK_FRAME("Book","update",0xf844b78e,"Book.update","Book.hx",52,0x4a7d2615)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(53)
		Float tmp = this->health;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(53)
		bool tmp1 = (tmp <= (int)0);		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(53)
		if ((tmp1)){
			HX_STACK_LINE(54)
			::flixel::animation::FlxAnimationController tmp2 = this->animation;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(54)
			tmp2->play(HX_HCSTRING("knock","\x14","\x1b","\xea","\xe4"),null(),null(),null());
		}
		HX_STACK_LINE(57)
		Float tmp2 = elapsed;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(57)
		this->super::update(tmp2);
	}
return null();
}



Book_obj::Book_obj()
{
}

void Book_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Book);
	HX_MARK_MEMBER_NAME(player,"player");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Book_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(player,"player");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Book_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"stateChange") ) { return stateChange_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Book_obj::__SetField(const ::String &inName,const Dynamic &inValue,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::PlayerBoxer >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Book_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"));
	super::__GetFields(outFields);
};

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::PlayerBoxer*/ ,(int)offsetof(Book_obj,player),HX_HCSTRING("player","\x61","\xeb","\xb8","\x37")},
	{ hx::fsUnknown, 0, null()}
};
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"),
	HX_HCSTRING("stateChange","\x61","\xde","\xfb","\x31"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Book_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Book_obj::__mClass,"__mClass");
};

#endif

hx::Class Book_obj::__mClass;

void Book_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("Book","\x49","\xb0","\xf4","\x2b");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< Book_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

