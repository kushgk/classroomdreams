#include <hxcpp.h>

#include "hxMath.h"
#ifndef INCLUDED_ChemLose
#include <ChemLose.h>
#endif
#ifndef INCLUDED_ChemState
#include <ChemState.h>
#endif
#ifndef INCLUDED_DDRState
#include <DDRState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimation
#include <flixel/animation/FlxAnimation.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxBaseAnimation
#include <flixel/animation/FlxBaseAnimation.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_FlxKeyManager
#include <flixel/input/FlxKeyManager.h>
#endif
#ifndef INCLUDED_flixel_input_IFlxInputManager
#include <flixel/input/IFlxInputManager.h>
#endif
#ifndef INCLUDED_flixel_input_keyboard_FlxKeyboard
#include <flixel/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_math_FlxPoint
#include <flixel/math/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSoundGroup
#include <flixel/system/FlxSoundGroup.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCollision
#include <flixel/util/FlxCollision.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxDestroyable
#include <flixel/util/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_IFlxPooled
#include <flixel/util/IFlxPooled.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObject
#include <openfl/_legacy/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_DisplayObjectContainer
#include <openfl/_legacy/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_IBitmapDrawable
#include <openfl/_legacy/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_InteractiveObject
#include <openfl/_legacy/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__legacy_display_Sprite
#include <openfl/_legacy/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_EventDispatcher
#include <openfl/_legacy/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__legacy_events_IEventDispatcher
#include <openfl/_legacy/events/IEventDispatcher.h>
#endif

Void ChemState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("ChemState","new",0x66f78f56,"ChemState.new","ChemState.hx",11,0x76b42cda)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(11)
	Dynamic tmp = MaxSize;		HX_STACK_VAR(tmp,"tmp");
	HX_STACK_LINE(11)
	super::__construct(tmp);
}
;
	return null();
}

//ChemState_obj::~ChemState_obj() { }

Dynamic ChemState_obj::__CreateEmpty() { return  new ChemState_obj; }
hx::ObjectPtr< ChemState_obj > ChemState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< ChemState_obj > _result_ = new ChemState_obj();
	_result_->__construct(MaxSize);
	return _result_;}

Dynamic ChemState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ChemState_obj > _result_ = new ChemState_obj();
	_result_->__construct(inArgs[0]);
	return _result_;}

Void ChemState_obj::create( ){
{
		HX_STACK_FRAME("ChemState","create",0xd670e5c6,"ChemState.create","ChemState.hx",21,0x76b42cda)
		HX_STACK_THIS(this)
		HX_STACK_LINE(22)
		this->super::create();
		HX_STACK_LINE(23)
		::flixel::FlxSprite tmp = ::flixel::FlxSprite_obj::__new((int)0,(int)0,HX_HCSTRING("assets/images/Level3/Stage31.png","\x62","\x41","\x03","\x3b"));		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(23)
		this->bg1 = tmp;
		HX_STACK_LINE(24)
		::flixel::FlxSprite tmp1 = ::flixel::FlxSprite_obj::__new((int)0,(int)-240,HX_HCSTRING("assets/images/Level3/Stage31.png","\x62","\x41","\x03","\x3b"));		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(24)
		this->bg2 = tmp1;
		HX_STACK_LINE(25)
		::flixel::FlxSprite tmp2 = this->bg1;		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(25)
		this->add(tmp2);
		HX_STACK_LINE(26)
		::flixel::FlxSprite tmp3 = this->bg2;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(26)
		this->add(tmp3);
		HX_STACK_LINE(27)
		this->floor = (int)0;
		HX_STACK_LINE(30)
		int tmp4 = (int)176;		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(30)
		::flixel::FlxSprite tmp5 = ::flixel::FlxSprite_obj::__new((int)160,tmp4,null());		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(30)
		this->player = tmp5;
		HX_STACK_LINE(31)
		::flixel::FlxSprite tmp6 = this->player;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(31)
		tmp6->loadGraphic(HX_HCSTRING("assets/images/Level3/ClimbingSS.png","\x3d","\x8a","\xb1","\x12"),true,(int)64,(int)64,null(),null());
		HX_STACK_LINE(32)
		::flixel::FlxSprite tmp7 = this->player;		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(32)
		tmp7->animation->add(HX_HCSTRING("upClimb","\x9a","\xa4","\xd3","\xa4"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)2).Add((int)1),(int)8,true,null(),null());
		HX_STACK_LINE(33)
		::flixel::FlxSprite tmp8 = this->player;		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(33)
		tmp8->animation->add(HX_HCSTRING("downClimb","\x33","\x59","\x74","\x5e"),Array_obj< int >::__new().Add((int)3).Add((int)2).Add((int)1).Add((int)0).Add((int)1).Add((int)2),(int)8,true,null(),null());
		HX_STACK_LINE(34)
		::flixel::FlxSprite tmp9 = this->player;		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(34)
		tmp9->animation->add(HX_HCSTRING("rightClimb","\x79","\x6b","\x23","\x4b"),Array_obj< int >::__new().Add((int)4).Add((int)5).Add((int)6).Add((int)5),(int)8,true,null(),null());
		HX_STACK_LINE(35)
		::flixel::FlxSprite tmp10 = this->player;		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(35)
		tmp10->animation->add(HX_HCSTRING("leftClimb","\x6e","\x73","\x3e","\x5d"),Array_obj< int >::__new().Add((int)6).Add((int)5).Add((int)4).Add((int)5),(int)8,true,null(),null());
		HX_STACK_LINE(36)
		::flixel::FlxSprite tmp11 = this->player;		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(36)
		this->add(tmp11);
		HX_STACK_LINE(38)
		::flixel::group::FlxTypedGroup tmp12 = ::flixel::group::FlxTypedGroup_obj::__new((int)999);		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(38)
		this->obstacles = tmp12;
		HX_STACK_LINE(39)
		::flixel::group::FlxTypedGroup tmp13 = this->obstacles;		HX_STACK_VAR(tmp13,"tmp13");
		HX_STACK_LINE(39)
		this->add(tmp13);
		HX_STACK_LINE(41)
		int tmp14 = (int)288;		HX_STACK_VAR(tmp14,"tmp14");
		HX_STACK_LINE(41)
		Float tmp15 = (Float(tmp14) / Float((int)2));		HX_STACK_VAR(tmp15,"tmp15");
		HX_STACK_LINE(41)
		::flixel::FlxSprite tmp16 = ::flixel::FlxSprite_obj::__new(tmp15,(int)-64,null());		HX_STACK_VAR(tmp16,"tmp16");
		HX_STACK_LINE(41)
		this->coin = tmp16;
		HX_STACK_LINE(42)
		::flixel::FlxSprite tmp17 = this->coin;		HX_STACK_VAR(tmp17,"tmp17");
		HX_STACK_LINE(42)
		tmp17->loadGraphic(HX_HCSTRING("assets/images/Level3/CoinSS.png","\x41","\xfd","\x74","\x42"),true,(int)32,(int)32,null(),null());
		HX_STACK_LINE(43)
		::flixel::FlxSprite tmp18 = this->coin;		HX_STACK_VAR(tmp18,"tmp18");
		HX_STACK_LINE(43)
		tmp18->animation->add(HX_HCSTRING("spin","\xc2","\xe1","\x58","\x4c"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)2).Add((int)1),(int)8,true,null(),null());
		HX_STACK_LINE(45)
		::flixel::_system::frontEnds::SoundFrontEnd tmp19 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp19,"tmp19");
		HX_STACK_LINE(45)
		tmp19->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/drowssy2.wav","\xe3","\xba","\x5d","\x48"),(int)1,true,null());
	}
return null();
}


Void ChemState_obj::update( Float elapsed){
{
		HX_STACK_FRAME("ChemState","update",0xe16704d3,"ChemState.update","ChemState.hx",48,0x76b42cda)
		HX_STACK_THIS(this)
		HX_STACK_ARG(elapsed,"elapsed")
		HX_STACK_LINE(50)
		::flixel::_system::frontEnds::SoundFrontEnd tmp = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(50)
		::flixel::_system::FlxSound tmp1 = tmp->__Field(HX_HCSTRING("music","\xa5","\xd0","\x5a","\x10"), hx::paccDynamic );		HX_STACK_VAR(tmp1,"tmp1");
		HX_STACK_LINE(50)
		bool tmp2 = (tmp1 == null());		HX_STACK_VAR(tmp2,"tmp2");
		HX_STACK_LINE(50)
		if ((tmp2)){
			HX_STACK_LINE(52)
			::flixel::_system::frontEnds::SoundFrontEnd tmp3 = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(52)
			tmp3->__Field(HX_HCSTRING("playMusic","\x11","\xfe","\x3e","\x31"), hx::paccDynamic )(HX_HCSTRING("assets/music/drowsy2.wav","\x9e","\xa3","\x51","\xc6"),(int)1,true,null());
		}
		HX_STACK_LINE(55)
		::flixel::FlxSprite tmp3 = this->player;		HX_STACK_VAR(tmp3,"tmp3");
		HX_STACK_LINE(55)
		::flixel::group::FlxTypedGroup tmp4 = this->obstacles;		HX_STACK_VAR(tmp4,"tmp4");
		HX_STACK_LINE(55)
		Dynamic tmp5 = this->lose_dyn();		HX_STACK_VAR(tmp5,"tmp5");
		HX_STACK_LINE(55)
		::flixel::FlxG_obj::overlap(tmp3,tmp4,tmp5,null());
		HX_STACK_LINE(57)
		::flixel::FlxSprite tmp6 = this->bg1;		HX_STACK_VAR(tmp6,"tmp6");
		HX_STACK_LINE(57)
		Float tmp7 = tmp6->y;		HX_STACK_VAR(tmp7,"tmp7");
		HX_STACK_LINE(57)
		bool tmp8 = (tmp7 > (int)240);		HX_STACK_VAR(tmp8,"tmp8");
		HX_STACK_LINE(57)
		if ((tmp8)){
			HX_STACK_LINE(58)
			::flixel::FlxSprite tmp9 = this->bg1;		HX_STACK_VAR(tmp9,"tmp9");
			HX_STACK_LINE(58)
			tmp9->set_y((int)-240);
			HX_STACK_LINE(59)
			(this->floor)++;
		}
		HX_STACK_LINE(62)
		::flixel::FlxSprite tmp9 = this->bg2;		HX_STACK_VAR(tmp9,"tmp9");
		HX_STACK_LINE(62)
		Float tmp10 = tmp9->y;		HX_STACK_VAR(tmp10,"tmp10");
		HX_STACK_LINE(62)
		bool tmp11 = (tmp10 > (int)240);		HX_STACK_VAR(tmp11,"tmp11");
		HX_STACK_LINE(62)
		if ((tmp11)){
			HX_STACK_LINE(63)
			::flixel::FlxSprite tmp12 = this->bg2;		HX_STACK_VAR(tmp12,"tmp12");
			HX_STACK_LINE(63)
			tmp12->set_y((int)-240);
			HX_STACK_LINE(64)
			(this->floor)++;
		}
		HX_STACK_LINE(67)
		::flixel::FlxSprite tmp12 = this->bg1;		HX_STACK_VAR(tmp12,"tmp12");
		HX_STACK_LINE(67)
		Float tmp13 = tmp12->y;		HX_STACK_VAR(tmp13,"tmp13");
		HX_STACK_LINE(67)
		bool tmp14 = (tmp13 < (int)-240);		HX_STACK_VAR(tmp14,"tmp14");
		HX_STACK_LINE(67)
		if ((tmp14)){
			HX_STACK_LINE(68)
			::flixel::FlxSprite tmp15 = this->bg1;		HX_STACK_VAR(tmp15,"tmp15");
			HX_STACK_LINE(68)
			tmp15->set_y((int)240);
			HX_STACK_LINE(69)
			(this->floor)--;
		}
		HX_STACK_LINE(72)
		::flixel::FlxSprite tmp15 = this->bg2;		HX_STACK_VAR(tmp15,"tmp15");
		HX_STACK_LINE(72)
		Float tmp16 = tmp15->y;		HX_STACK_VAR(tmp16,"tmp16");
		HX_STACK_LINE(72)
		bool tmp17 = (tmp16 < (int)-240);		HX_STACK_VAR(tmp17,"tmp17");
		HX_STACK_LINE(72)
		if ((tmp17)){
			HX_STACK_LINE(73)
			::flixel::FlxSprite tmp18 = this->bg2;		HX_STACK_VAR(tmp18,"tmp18");
			HX_STACK_LINE(73)
			tmp18->set_y((int)240);
			HX_STACK_LINE(74)
			(this->floor)--;
		}
		HX_STACK_LINE(77)
		::flixel::input::keyboard::FlxKeyboard tmp18 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp18,"tmp18");
		HX_STACK_LINE(77)
		bool tmp19 = tmp18->checkKeyArrayState(Array_obj< int >::__new().Add((int)38).Add((int)87),(int)1);		HX_STACK_VAR(tmp19,"tmp19");
		HX_STACK_LINE(77)
		if ((tmp19)){
			HX_STACK_LINE(79)
			::flixel::FlxSprite tmp20 = this->bg1;		HX_STACK_VAR(tmp20,"tmp20");
			HX_STACK_LINE(79)
			tmp20->velocity->set_y((int)50);
			HX_STACK_LINE(80)
			::flixel::FlxSprite tmp21 = this->bg2;		HX_STACK_VAR(tmp21,"tmp21");
			HX_STACK_LINE(80)
			tmp21->velocity->set_y((int)50);
			HX_STACK_LINE(81)
			::flixel::FlxSprite tmp22 = this->coin;		HX_STACK_VAR(tmp22,"tmp22");
			HX_STACK_LINE(81)
			tmp22->velocity->set_y((int)50);
			HX_STACK_LINE(82)
			::flixel::group::FlxTypedGroup tmp23 = this->obstacles;		HX_STACK_VAR(tmp23,"tmp23");

			HX_BEGIN_LOCAL_FUNC_S0(hx::LocalFunc,_Function_2_1)
			int __ArgCount() const { return 1; }
			Void run(::flixel::FlxBasic member){
				HX_STACK_FRAME("*","_Function_2_1",0x5201af78,"*._Function_2_1","ChemState.hx",82,0x76b42cda)
				HX_STACK_ARG(member,"member")
				{
					HX_STACK_LINE(83)
					::flixel::FlxSprite obj = ((::flixel::FlxSprite)(member));		HX_STACK_VAR(obj,"obj");
					HX_STACK_LINE(84)
					{
						HX_STACK_LINE(84)
						::flixel::FlxSprite _g = obj;		HX_STACK_VAR(_g,"_g");
						HX_STACK_LINE(84)
						Float tmp24 = (_g->y + ((Float)0.6));		HX_STACK_VAR(tmp24,"tmp24");
						HX_STACK_LINE(84)
						_g->set_y(tmp24);
					}
				}
				return null();
			}
			HX_END_LOCAL_FUNC1((void))

			HX_STACK_LINE(82)
			tmp23->forEach( Dynamic(new _Function_2_1()),null());
			HX_STACK_LINE(86)
			::flixel::FlxSprite tmp24 = this->player;		HX_STACK_VAR(tmp24,"tmp24");
			HX_STACK_LINE(86)
			tmp24->animation->play(HX_HCSTRING("upClimb","\x9a","\xa4","\xd3","\xa4"),null(),null(),null());
		}
		else{
			HX_STACK_LINE(87)
			::flixel::input::keyboard::FlxKeyboard tmp20 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp20,"tmp20");
			HX_STACK_LINE(87)
			bool tmp21 = tmp20->checkKeyArrayState(Array_obj< int >::__new().Add((int)40).Add((int)83),(int)1);		HX_STACK_VAR(tmp21,"tmp21");
			HX_STACK_LINE(87)
			if ((tmp21)){
				HX_STACK_LINE(89)
				::flixel::FlxSprite tmp22 = this->bg1;		HX_STACK_VAR(tmp22,"tmp22");
				HX_STACK_LINE(89)
				tmp22->velocity->set_y((int)-50);
				HX_STACK_LINE(90)
				::flixel::FlxSprite tmp23 = this->bg2;		HX_STACK_VAR(tmp23,"tmp23");
				HX_STACK_LINE(90)
				tmp23->velocity->set_y((int)-50);
				HX_STACK_LINE(91)
				::flixel::FlxSprite tmp24 = this->coin;		HX_STACK_VAR(tmp24,"tmp24");
				HX_STACK_LINE(91)
				tmp24->velocity->set_y((int)-50);
				HX_STACK_LINE(92)
				::flixel::group::FlxTypedGroup tmp25 = this->obstacles;		HX_STACK_VAR(tmp25,"tmp25");

				HX_BEGIN_LOCAL_FUNC_S0(hx::LocalFunc,_Function_3_1)
				int __ArgCount() const { return 1; }
				Void run(::flixel::FlxBasic member){
					HX_STACK_FRAME("*","_Function_3_1",0x520271b9,"*._Function_3_1","ChemState.hx",92,0x76b42cda)
					HX_STACK_ARG(member,"member")
					{
						HX_STACK_LINE(93)
						::flixel::FlxSprite obj = ((::flixel::FlxSprite)(member));		HX_STACK_VAR(obj,"obj");
						HX_STACK_LINE(94)
						{
							HX_STACK_LINE(94)
							::flixel::FlxSprite _g = obj;		HX_STACK_VAR(_g,"_g");
							HX_STACK_LINE(94)
							Float tmp26 = (_g->y - ((Float)0.6));		HX_STACK_VAR(tmp26,"tmp26");
							HX_STACK_LINE(94)
							_g->set_y(tmp26);
						}
					}
					return null();
				}
				HX_END_LOCAL_FUNC1((void))

				HX_STACK_LINE(92)
				tmp25->forEach( Dynamic(new _Function_3_1()),null());
				HX_STACK_LINE(96)
				::flixel::FlxSprite tmp26 = this->player;		HX_STACK_VAR(tmp26,"tmp26");
				HX_STACK_LINE(96)
				tmp26->animation->play(HX_HCSTRING("upClimb","\x9a","\xa4","\xd3","\xa4"),null(),null(),null());
			}
			else{
				HX_STACK_LINE(98)
				::flixel::FlxSprite tmp22 = this->bg1;		HX_STACK_VAR(tmp22,"tmp22");
				HX_STACK_LINE(98)
				tmp22->velocity->set_y((int)0);
				HX_STACK_LINE(99)
				::flixel::FlxSprite tmp23 = this->bg2;		HX_STACK_VAR(tmp23,"tmp23");
				HX_STACK_LINE(99)
				tmp23->velocity->set_y((int)0);
				HX_STACK_LINE(100)
				::flixel::FlxSprite tmp24 = this->coin;		HX_STACK_VAR(tmp24,"tmp24");
				HX_STACK_LINE(100)
				tmp24->velocity->set_y((int)0);
				HX_STACK_LINE(101)
				::flixel::input::keyboard::FlxKeyboard tmp25 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp25,"tmp25");
				HX_STACK_LINE(101)
				bool tmp26 = tmp25->checkKeyArrayState(Array_obj< int >::__new().Add((int)37).Add((int)65),(int)1);		HX_STACK_VAR(tmp26,"tmp26");
				HX_STACK_LINE(101)
				bool tmp27;		HX_STACK_VAR(tmp27,"tmp27");
				HX_STACK_LINE(101)
				if ((tmp26)){
					HX_STACK_LINE(101)
					::flixel::FlxSprite tmp28 = this->player;		HX_STACK_VAR(tmp28,"tmp28");
					HX_STACK_LINE(101)
					::flixel::FlxSprite tmp29 = tmp28;		HX_STACK_VAR(tmp29,"tmp29");
					HX_STACK_LINE(101)
					Float tmp30 = tmp29->x;		HX_STACK_VAR(tmp30,"tmp30");
					HX_STACK_LINE(101)
					tmp27 = (tmp30 > (int)83);
				}
				else{
					HX_STACK_LINE(101)
					tmp27 = false;
				}
				HX_STACK_LINE(101)
				if ((tmp27)){
					HX_STACK_LINE(103)
					::flixel::FlxSprite tmp28 = this->player;		HX_STACK_VAR(tmp28,"tmp28");
					HX_STACK_LINE(103)
					tmp28->velocity->set_x((int)-50);
					HX_STACK_LINE(104)
					::flixel::FlxSprite tmp29 = this->player;		HX_STACK_VAR(tmp29,"tmp29");
					HX_STACK_LINE(104)
					tmp29->animation->play(HX_HCSTRING("leftClimb","\x6e","\x73","\x3e","\x5d"),null(),null(),null());
				}
				else{
					HX_STACK_LINE(105)
					::flixel::input::keyboard::FlxKeyboard tmp28 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp28,"tmp28");
					HX_STACK_LINE(105)
					bool tmp29 = tmp28->checkKeyArrayState(Array_obj< int >::__new().Add((int)39).Add((int)68),(int)1);		HX_STACK_VAR(tmp29,"tmp29");
					HX_STACK_LINE(105)
					bool tmp30;		HX_STACK_VAR(tmp30,"tmp30");
					HX_STACK_LINE(105)
					if ((tmp29)){
						HX_STACK_LINE(105)
						::flixel::FlxSprite tmp31 = this->player;		HX_STACK_VAR(tmp31,"tmp31");
						HX_STACK_LINE(105)
						::flixel::FlxSprite tmp32 = tmp31;		HX_STACK_VAR(tmp32,"tmp32");
						HX_STACK_LINE(105)
						Float tmp33 = tmp32->x;		HX_STACK_VAR(tmp33,"tmp33");
						HX_STACK_LINE(105)
						::flixel::FlxSprite tmp34 = this->player;		HX_STACK_VAR(tmp34,"tmp34");
						HX_STACK_LINE(105)
						::flixel::FlxSprite tmp35 = tmp34;		HX_STACK_VAR(tmp35,"tmp35");
						HX_STACK_LINE(105)
						Float tmp36 = tmp35->get_width();		HX_STACK_VAR(tmp36,"tmp36");
						HX_STACK_LINE(105)
						Float tmp37 = tmp36;		HX_STACK_VAR(tmp37,"tmp37");
						HX_STACK_LINE(105)
						Float tmp38 = ((int)235 - tmp37);		HX_STACK_VAR(tmp38,"tmp38");
						HX_STACK_LINE(105)
						Float tmp39 = tmp38;		HX_STACK_VAR(tmp39,"tmp39");
						HX_STACK_LINE(105)
						tmp30 = (tmp33 < tmp39);
					}
					else{
						HX_STACK_LINE(105)
						tmp30 = false;
					}
					HX_STACK_LINE(105)
					if ((tmp30)){
						HX_STACK_LINE(107)
						::flixel::FlxSprite tmp31 = this->player;		HX_STACK_VAR(tmp31,"tmp31");
						HX_STACK_LINE(107)
						tmp31->velocity->set_x((int)50);
						HX_STACK_LINE(108)
						::flixel::FlxSprite tmp32 = this->player;		HX_STACK_VAR(tmp32,"tmp32");
						HX_STACK_LINE(108)
						tmp32->animation->play(HX_HCSTRING("rightClimb","\x79","\x6b","\x23","\x4b"),null(),null(),null());
					}
					else{
						HX_STACK_LINE(110)
						::flixel::FlxSprite tmp31 = this->player;		HX_STACK_VAR(tmp31,"tmp31");
						HX_STACK_LINE(110)
						tmp31->velocity->set_x((int)0);
						HX_STACK_LINE(111)
						{
							HX_STACK_LINE(111)
							::flixel::FlxSprite tmp32 = this->player;		HX_STACK_VAR(tmp32,"tmp32");
							HX_STACK_LINE(111)
							::flixel::animation::FlxAnimationController _this = tmp32->animation;		HX_STACK_VAR(_this,"_this");
							HX_STACK_LINE(111)
							bool tmp33 = (_this->_curAnim != null());		HX_STACK_VAR(tmp33,"tmp33");
							HX_STACK_LINE(111)
							if ((tmp33)){
								HX_STACK_LINE(111)
								_this->_curAnim->pause();
							}
						}
					}
				}
			}
		}
		HX_STACK_LINE(115)
		::flixel::input::keyboard::FlxKeyboard tmp20 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp20,"tmp20");
		HX_STACK_LINE(115)
		bool tmp21 = tmp20->checkKeyArrayState(Array_obj< int >::__new().Add((int)37).Add((int)65),(int)1);		HX_STACK_VAR(tmp21,"tmp21");
		HX_STACK_LINE(115)
		bool tmp22 = !(tmp21);		HX_STACK_VAR(tmp22,"tmp22");
		HX_STACK_LINE(115)
		bool tmp23;		HX_STACK_VAR(tmp23,"tmp23");
		HX_STACK_LINE(115)
		if ((tmp22)){
			HX_STACK_LINE(115)
			::flixel::input::keyboard::FlxKeyboard tmp24 = ::flixel::FlxG_obj::keys;		HX_STACK_VAR(tmp24,"tmp24");
			HX_STACK_LINE(115)
			::flixel::input::keyboard::FlxKeyboard tmp25 = tmp24;		HX_STACK_VAR(tmp25,"tmp25");
			HX_STACK_LINE(115)
			tmp23 = tmp25->checkKeyArrayState(Array_obj< int >::__new().Add((int)39).Add((int)68),(int)1);
		}
		else{
			HX_STACK_LINE(115)
			tmp23 = true;
		}
		HX_STACK_LINE(115)
		bool tmp24 = !(tmp23);		HX_STACK_VAR(tmp24,"tmp24");
		HX_STACK_LINE(115)
		if ((tmp24)){
			HX_STACK_LINE(116)
			::flixel::FlxSprite tmp25 = this->player;		HX_STACK_VAR(tmp25,"tmp25");
			HX_STACK_LINE(116)
			tmp25->velocity->set_x((int)0);
		}
		HX_STACK_LINE(119)
		int tmp25 = this->floor;		HX_STACK_VAR(tmp25,"tmp25");
		HX_STACK_LINE(119)
		bool tmp26 = (tmp25 == (int)4);		HX_STACK_VAR(tmp26,"tmp26");
		HX_STACK_LINE(119)
		if ((tmp26)){
			HX_STACK_LINE(120)
			::flixel::FlxSprite tmp27 = this->bg2;		HX_STACK_VAR(tmp27,"tmp27");
			HX_STACK_LINE(120)
			tmp27->loadGraphic(HX_HCSTRING("assets/images/Level3/Stage32.png","\xe3","\xd5","\x69","\xce"),null(),null(),null(),null(),null());
			HX_STACK_LINE(121)
			::flixel::FlxSprite tmp28 = this->coin;		HX_STACK_VAR(tmp28,"tmp28");
			HX_STACK_LINE(121)
			this->add(tmp28);
			HX_STACK_LINE(122)
			::flixel::FlxSprite tmp29 = this->coin;		HX_STACK_VAR(tmp29,"tmp29");
			HX_STACK_LINE(122)
			tmp29->animation->play(HX_HCSTRING("spin","\xc2","\xe1","\x58","\x4c"),null(),null(),null());
			HX_STACK_LINE(123)
			this->floor = (int)10;
		}
		HX_STACK_LINE(127)
		int tmp27 = this->floor;		HX_STACK_VAR(tmp27,"tmp27");
		HX_STACK_LINE(127)
		bool tmp28 = (tmp27 == (int)11);		HX_STACK_VAR(tmp28,"tmp28");
		HX_STACK_LINE(127)
		if ((tmp28)){
			HX_STACK_LINE(128)
			::DDRState tmp29 = ::DDRState_obj::__new(null());		HX_STACK_VAR(tmp29,"tmp29");
			HX_STACK_LINE(128)
			::flixel::FlxState nextState = tmp29;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(128)
			::flixel::FlxGame tmp30 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp30,"tmp30");
			HX_STACK_LINE(128)
			::flixel::FlxState tmp31 = nextState;		HX_STACK_VAR(tmp31,"tmp31");
			HX_STACK_LINE(128)
			bool tmp32 = tmp30->_state->switchTo(tmp31);		HX_STACK_VAR(tmp32,"tmp32");
			HX_STACK_LINE(128)
			if ((tmp32)){
				HX_STACK_LINE(128)
				::flixel::FlxGame tmp33 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp33,"tmp33");
				HX_STACK_LINE(128)
				tmp33->_requestedState = nextState;
			}
		}
		HX_STACK_LINE(131)
		Float tmp29 = ::Math_obj::random();		HX_STACK_VAR(tmp29,"tmp29");
		HX_STACK_LINE(131)
		bool tmp30 = (tmp29 < ((Float)0.009));		HX_STACK_VAR(tmp30,"tmp30");
		HX_STACK_LINE(131)
		if ((tmp30)){
			HX_STACK_LINE(132)
			Float tmp31 = ::Math_obj::random();		HX_STACK_VAR(tmp31,"tmp31");
			HX_STACK_LINE(132)
			int tmp32 = (int)140;		HX_STACK_VAR(tmp32,"tmp32");
			HX_STACK_LINE(132)
			Float tmp33 = (tmp31 * tmp32);		HX_STACK_VAR(tmp33,"tmp33");
			HX_STACK_LINE(132)
			Float tmp34 = (tmp33 + (int)83);		HX_STACK_VAR(tmp34,"tmp34");
			HX_STACK_LINE(132)
			::flixel::FlxSprite tmp35 = ::flixel::FlxSprite_obj::__new(tmp34,(int)0,null());		HX_STACK_VAR(tmp35,"tmp35");
			HX_STACK_LINE(132)
			::flixel::FlxSprite poison = tmp35;		HX_STACK_VAR(poison,"poison");
			HX_STACK_LINE(133)
			poison->loadGraphic(HX_HCSTRING("assets/images/Level3/PoisonSS.png","\x38","\x43","\xa6","\x8f"),true,(int)16,(int)16,null(),null());
			HX_STACK_LINE(134)
			poison->animation->add(HX_HCSTRING("fall","\x7b","\xbc","\xb5","\x43"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)5,true,null(),null());
			HX_STACK_LINE(135)
			::flixel::group::FlxTypedGroup tmp36 = this->obstacles;		HX_STACK_VAR(tmp36,"tmp36");
			HX_STACK_LINE(135)
			::flixel::FlxSprite tmp37 = poison;		HX_STACK_VAR(tmp37,"tmp37");
			HX_STACK_LINE(135)
			tmp36->add(tmp37);
			HX_STACK_LINE(136)
			poison->velocity->set_y((int)30);
			HX_STACK_LINE(137)
			poison->acceleration->set_y((int)10);
			HX_STACK_LINE(138)
			poison->animation->play(HX_HCSTRING("fall","\x7b","\xbc","\xb5","\x43"),null(),null(),null());
		}
		HX_STACK_LINE(141)
		Float tmp31 = elapsed;		HX_STACK_VAR(tmp31,"tmp31");
		HX_STACK_LINE(141)
		this->super::update(tmp31);
	}
return null();
}


Void ChemState_obj::lose( ::flixel::FlxSprite player,::flixel::FlxSprite obstacle){
{
		HX_STACK_FRAME("ChemState","lose",0xb05b021f,"ChemState.lose","ChemState.hx",144,0x76b42cda)
		HX_STACK_THIS(this)
		HX_STACK_ARG(player,"player")
		HX_STACK_ARG(obstacle,"obstacle")
		HX_STACK_LINE(145)
		bool tmp;		HX_STACK_VAR(tmp,"tmp");
		HX_STACK_LINE(145)
		{
			HX_STACK_LINE(145)
			int AlphaTolerance = (int)255;		HX_STACK_VAR(AlphaTolerance,"AlphaTolerance");
			HX_STACK_LINE(145)
			::flixel::FlxSprite tmp1 = player;		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(145)
			::flixel::FlxSprite tmp2 = obstacle;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(145)
			int tmp3 = AlphaTolerance;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(145)
			tmp = ::flixel::util::FlxCollision_obj::pixelPerfectCheck(tmp1,tmp2,tmp3,null());
		}
		HX_STACK_LINE(145)
		if ((tmp)){
			HX_STACK_LINE(146)
			::ChemLose tmp1 = ::ChemLose_obj::__new(null());		HX_STACK_VAR(tmp1,"tmp1");
			HX_STACK_LINE(146)
			::flixel::FlxState nextState = tmp1;		HX_STACK_VAR(nextState,"nextState");
			HX_STACK_LINE(146)
			::flixel::FlxGame tmp2 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp2,"tmp2");
			HX_STACK_LINE(146)
			::flixel::FlxState tmp3 = nextState;		HX_STACK_VAR(tmp3,"tmp3");
			HX_STACK_LINE(146)
			bool tmp4 = tmp2->_state->switchTo(tmp3);		HX_STACK_VAR(tmp4,"tmp4");
			HX_STACK_LINE(146)
			if ((tmp4)){
				HX_STACK_LINE(146)
				::flixel::FlxGame tmp5 = ::flixel::FlxG_obj::game;		HX_STACK_VAR(tmp5,"tmp5");
				HX_STACK_LINE(146)
				tmp5->_requestedState = nextState;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(ChemState_obj,lose,(void))


ChemState_obj::ChemState_obj()
{
}

void ChemState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ChemState);
	HX_MARK_MEMBER_NAME(bg1,"bg1");
	HX_MARK_MEMBER_NAME(bg2,"bg2");
	HX_MARK_MEMBER_NAME(floor,"floor");
	HX_MARK_MEMBER_NAME(player,"player");
	HX_MARK_MEMBER_NAME(obstacles,"obstacles");
	HX_MARK_MEMBER_NAME(coin,"coin");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void ChemState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(bg1,"bg1");
	HX_VISIT_MEMBER_NAME(bg2,"bg2");
	HX_VISIT_MEMBER_NAME(floor,"floor");
	HX_VISIT_MEMBER_NAME(player,"player");
	HX_VISIT_MEMBER_NAME(obstacles,"obstacles");
	HX_VISIT_MEMBER_NAME(coin,"coin");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic ChemState_obj::__Field(const ::String &inName,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"bg1") ) { return bg1; }
		if (HX_FIELD_EQ(inName,"bg2") ) { return bg2; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { return coin; }
		if (HX_FIELD_EQ(inName,"lose") ) { return lose_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"floor") ) { return floor; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"obstacles") ) { return obstacles; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ChemState_obj::__SetField(const ::String &inName,const Dynamic &inValue,hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"bg1") ) { bg1=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"bg2") ) { bg2=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { coin=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"floor") ) { floor=inValue.Cast< int >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"obstacles") ) { obstacles=inValue.Cast< ::flixel::group::FlxTypedGroup >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ChemState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_HCSTRING("bg1","\xcc","\xb6","\x4a","\x00"));
	outFields->push(HX_HCSTRING("bg2","\xcd","\xb6","\x4a","\x00"));
	outFields->push(HX_HCSTRING("floor","\xcc","\xd1","\x96","\x02"));
	outFields->push(HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"));
	outFields->push(HX_HCSTRING("obstacles","\x44","\x1b","\x27","\x87"));
	outFields->push(HX_HCSTRING("coin","\x91","\xb5","\xc4","\x41"));
	super::__GetFields(outFields);
};

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ChemState_obj,bg1),HX_HCSTRING("bg1","\xcc","\xb6","\x4a","\x00")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ChemState_obj,bg2),HX_HCSTRING("bg2","\xcd","\xb6","\x4a","\x00")},
	{hx::fsInt,(int)offsetof(ChemState_obj,floor),HX_HCSTRING("floor","\xcc","\xd1","\x96","\x02")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ChemState_obj,player),HX_HCSTRING("player","\x61","\xeb","\xb8","\x37")},
	{hx::fsObject /*::flixel::group::FlxTypedGroup*/ ,(int)offsetof(ChemState_obj,obstacles),HX_HCSTRING("obstacles","\x44","\x1b","\x27","\x87")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ChemState_obj,coin),HX_HCSTRING("coin","\x91","\xb5","\xc4","\x41")},
	{ hx::fsUnknown, 0, null()}
};
static hx::StaticInfo *sStaticStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_HCSTRING("bg1","\xcc","\xb6","\x4a","\x00"),
	HX_HCSTRING("bg2","\xcd","\xb6","\x4a","\x00"),
	HX_HCSTRING("floor","\xcc","\xd1","\x96","\x02"),
	HX_HCSTRING("player","\x61","\xeb","\xb8","\x37"),
	HX_HCSTRING("obstacles","\x44","\x1b","\x27","\x87"),
	HX_HCSTRING("coin","\x91","\xb5","\xc4","\x41"),
	HX_HCSTRING("create","\xfc","\x66","\x0f","\x7c"),
	HX_HCSTRING("update","\x09","\x86","\x05","\x87"),
	HX_HCSTRING("lose","\xd5","\xa9","\xb7","\x47"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ChemState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ChemState_obj::__mClass,"__mClass");
};

#endif

hx::Class ChemState_obj::__mClass;

void ChemState_obj::__register()
{
	hx::Static(__mClass) = new hx::Class_obj();
	__mClass->mName = HX_HCSTRING("ChemState","\x64","\xec","\xc3","\x85");
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &hx::Class_obj::GetNoStaticField;
	__mClass->mSetStaticField = &hx::Class_obj::SetNoStaticField;
	__mClass->mMarkFunc = sMarkStatics;
	__mClass->mStatics = hx::Class_obj::dupFunctions(0 /* sStaticFields */);
	__mClass->mMembers = hx::Class_obj::dupFunctions(sMemberFields);
	__mClass->mCanCast = hx::TCanCast< ChemState_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = sStaticStorageInfo;
#endif
	hx::RegisterClass(__mClass->mName, __mClass);
}

