#ifndef INCLUDED_Book
#define INCLUDED_Book

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
HX_DECLARE_CLASS0(Book)
HX_DECLARE_CLASS0(PlayerBoxer)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Book_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef Book_obj OBJ_;
		Book_obj();
		Void __construct(::PlayerBoxer play);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="Book")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< Book_obj > __new(::PlayerBoxer play);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Book_obj();

		HX_DO_RTTI_ALL;
		Dynamic __Field(const ::String &inString, hx::PropertyAccess inCallProp);
		Dynamic __SetField(const ::String &inString,const Dynamic &inValue, hx::PropertyAccess inCallProp);
		void __GetFields(Array< ::String> &outFields);
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_HCSTRING("Book","\x49","\xb0","\xf4","\x2b"); }

		::PlayerBoxer player;
		virtual Void stateChange( ::String anim);
		Dynamic stateChange_dyn();

		virtual Void update( Float elapsed);

};


#endif /* INCLUDED_Book */ 
