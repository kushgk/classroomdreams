#ifndef INCLUDED_ChemState
#define INCLUDED_ChemState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
HX_DECLARE_CLASS0(ChemState)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  ChemState_obj : public ::flixel::FlxState_obj{
	public:
		typedef ::flixel::FlxState_obj super;
		typedef ChemState_obj OBJ_;
		ChemState_obj();
		Void __construct(Dynamic MaxSize);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="ChemState")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< ChemState_obj > __new(Dynamic MaxSize);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~ChemState_obj();

		HX_DO_RTTI_ALL;
		Dynamic __Field(const ::String &inString, hx::PropertyAccess inCallProp);
		Dynamic __SetField(const ::String &inString,const Dynamic &inValue, hx::PropertyAccess inCallProp);
		void __GetFields(Array< ::String> &outFields);
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_HCSTRING("ChemState","\x64","\xec","\xc3","\x85"); }

		::flixel::FlxSprite bg1;
		::flixel::FlxSprite bg2;
		int floor;
		::flixel::FlxSprite player;
		::flixel::group::FlxTypedGroup obstacles;
		::flixel::FlxSprite coin;
		virtual Void create( );

		virtual Void update( Float elapsed);

		virtual Void lose( ::flixel::FlxSprite player,::flixel::FlxSprite obstacle);
		Dynamic lose_dyn();

};


#endif /* INCLUDED_ChemState */ 
