#ifndef INCLUDED_DDRState
#define INCLUDED_DDRState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
HX_DECLARE_CLASS0(DDRState)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  DDRState_obj : public ::flixel::FlxState_obj{
	public:
		typedef ::flixel::FlxState_obj super;
		typedef DDRState_obj OBJ_;
		DDRState_obj();
		Void __construct(Dynamic MaxSize);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="DDRState")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< DDRState_obj > __new(Dynamic MaxSize);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~DDRState_obj();

		HX_DO_RTTI_ALL;
		Dynamic __Field(const ::String &inString, hx::PropertyAccess inCallProp);
		Dynamic __SetField(const ::String &inString,const Dynamic &inValue, hx::PropertyAccess inCallProp);
		void __GetFields(Array< ::String> &outFields);
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_HCSTRING("DDRState","\xdf","\x57","\x96","\x1d"); }

		::flixel::FlxSprite player;
		::flixel::FlxSprite lincoln;
		::flixel::FlxSprite napoleon;
		int beatCounter;
		Array< ::String > beats;
		::flixel::group::FlxTypedGroup dancers;
		::flixel::group::FlxTypedGroup prompts;
		::flixel::group::FlxTypedGroup outputs;
		int failure;
		virtual Void create( );

		virtual Void update( Float elapsed);

};


#endif /* INCLUDED_DDRState */ 
