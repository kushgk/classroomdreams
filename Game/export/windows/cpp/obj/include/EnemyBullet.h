#ifndef INCLUDED_EnemyBullet
#define INCLUDED_EnemyBullet

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
HX_DECLARE_CLASS0(EnemyBullet)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  EnemyBullet_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef EnemyBullet_obj OBJ_;
		EnemyBullet_obj();
		Void __construct(Dynamic x,Dynamic y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="EnemyBullet")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< EnemyBullet_obj > __new(Dynamic x,Dynamic y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~EnemyBullet_obj();

		HX_DO_RTTI_ALL;
		static void __register();
		::String __ToString() const { return HX_HCSTRING("EnemyBullet","\x8a","\x96","\x75","\xa0"); }

};


#endif /* INCLUDED_EnemyBullet */ 
