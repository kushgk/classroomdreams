#ifndef INCLUDED_Integral
#define INCLUDED_Integral

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
HX_DECLARE_CLASS0(Integral)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Integral_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef Integral_obj OBJ_;
		Integral_obj();
		Void __construct(Dynamic x,Dynamic y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="Integral")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< Integral_obj > __new(Dynamic x,Dynamic y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Integral_obj();

		HX_DO_RTTI_ALL;
		Dynamic __Field(const ::String &inString, hx::PropertyAccess inCallProp);
		Dynamic __SetField(const ::String &inString,const Dynamic &inValue, hx::PropertyAccess inCallProp);
		void __GetFields(Array< ::String> &outFields);
		static void __register();
		::String __ToString() const { return HX_HCSTRING("Integral","\x8c","\xd3","\xe5","\xfa"); }

		Float _shotClock;
		int _originalX;
		int stepCount;
		int dir;
		bool moving;
		virtual Void update( Float elapsed);

		virtual Void die( );
		Dynamic die_dyn();

		virtual Void suicide( ::String fummy);
		Dynamic suicide_dyn();

};


#endif /* INCLUDED_Integral */ 
