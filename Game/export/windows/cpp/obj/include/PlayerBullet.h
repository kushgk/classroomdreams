#ifndef INCLUDED_PlayerBullet
#define INCLUDED_PlayerBullet

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
HX_DECLARE_CLASS0(PlayerBullet)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  PlayerBullet_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef PlayerBullet_obj OBJ_;
		PlayerBullet_obj();
		Void __construct(Dynamic x,Dynamic y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="PlayerBullet")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< PlayerBullet_obj > __new(Dynamic x,Dynamic y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~PlayerBullet_obj();

		HX_DO_RTTI_ALL;
		static void __register();
		::String __ToString() const { return HX_HCSTRING("PlayerBullet","\xa3","\x7c","\x94","\x98"); }

};


#endif /* INCLUDED_PlayerBullet */ 
