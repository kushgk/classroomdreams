#ifndef INCLUDED_SpaceState
#define INCLUDED_SpaceState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
HX_DECLARE_CLASS0(PlayerShip)
HX_DECLARE_CLASS0(SpaceState)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,util,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  SpaceState_obj : public ::flixel::FlxState_obj{
	public:
		typedef ::flixel::FlxState_obj super;
		typedef SpaceState_obj OBJ_;
		SpaceState_obj();
		Void __construct(Dynamic MaxSize);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true,const char *inName="SpaceState")
			{ return hx::Object::operator new(inSize,inContainer,inName); }
		static hx::ObjectPtr< SpaceState_obj > __new(Dynamic MaxSize);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~SpaceState_obj();

		HX_DO_RTTI_ALL;
		Dynamic __Field(const ::String &inString, hx::PropertyAccess inCallProp);
		Dynamic __SetField(const ::String &inString,const Dynamic &inValue, hx::PropertyAccess inCallProp);
		void __GetFields(Array< ::String> &outFields);
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_HCSTRING("SpaceState","\x8b","\x4f","\xf1","\xdb"); }

		::PlayerShip player;
		::flixel::group::FlxTypedGroup enemies;
		::flixel::group::FlxTypedGroup playerBullets;
		::flixel::group::FlxTypedGroup enemyBullets;
		Float timer;
		virtual Void create( );

		virtual Void update( Float elapsed);

		virtual Void killEnemy( Dynamic enemy,Dynamic bullet);
		Dynamic killEnemy_dyn();

		virtual Void killPlayer( ::flixel::FlxSprite play,::flixel::FlxSprite bullet);
		Dynamic killPlayer_dyn();

		virtual Void enemyShoot( ::flixel::FlxBasic enem);
		Dynamic enemyShoot_dyn();

};


#endif /* INCLUDED_SpaceState */ 
