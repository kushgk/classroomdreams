package lime;


import lime.utils.Assets;


class AssetData {

	private static var initialized:Bool = false;
	
	public static var library = new #if haxe3 Map <String, #else Hash <#end LibraryType> ();
	public static var path = new #if haxe3 Map <String, #else Hash <#end String> ();
	public static var type = new #if haxe3 Map <String, #else Hash <#end AssetType> ();	
	
	public static function initialize():Void {
		
		if (!initialized) {
			
			path.set ("assets/data/data-goes-here.txt", "assets/data/data-goes-here.txt");
			type.set ("assets/data/data-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/GameOVer.png", "assets/images/GameOVer.png");
			type.set ("assets/images/GameOVer.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/images-go-here.txt", "assets/images/images-go-here.txt");
			type.set ("assets/images/images-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/Level1/BookSheet.png", "assets/images/Level1/BookSheet.png");
			type.set ("assets/images/Level1/BookSheet.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/Lv1UI.png", "assets/images/Level1/Lv1UI.png");
			type.set ("assets/images/Level1/Lv1UI.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/MCSS.png", "assets/images/Level1/MCSS.png");
			type.set ("assets/images/Level1/MCSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/MCSS2.png", "assets/images/Level1/MCSS2.png");
			type.set ("assets/images/Level1/MCSS2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/message1.png", "assets/images/Level1/message1.png");
			type.set ("assets/images/Level1/message1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/Period1.png", "assets/images/Level1/Period1.png");
			type.set ("assets/images/Level1/Period1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/Stage.png", "assets/images/Level1/Stage.png");
			type.set ("assets/images/Level1/Stage.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level1/StageOverlay.png", "assets/images/Level1/StageOverlay.png");
			type.set ("assets/images/Level1/StageOverlay.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/DivideSS.png", "assets/images/Level2/DivideSS.png");
			type.set ("assets/images/Level2/DivideSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/Enemybullet.png", "assets/images/Level2/Enemybullet.png");
			type.set ("assets/images/Level2/Enemybullet.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/IntegrateSS.png", "assets/images/Level2/IntegrateSS.png");
			type.set ("assets/images/Level2/IntegrateSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/message2.png", "assets/images/Level2/message2.png");
			type.set ("assets/images/Level2/message2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/MultiplySS.png", "assets/images/Level2/MultiplySS.png");
			type.set ("assets/images/Level2/MultiplySS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/Period2.png", "assets/images/Level2/Period2.png");
			type.set ("assets/images/Level2/Period2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/Playerbullet.png", "assets/images/Level2/Playerbullet.png");
			type.set ("assets/images/Level2/Playerbullet.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/PuffSS.png", "assets/images/Level2/PuffSS.png");
			type.set ("assets/images/Level2/PuffSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/SpaceshipSS.png", "assets/images/Level2/SpaceshipSS.png");
			type.set ("assets/images/Level2/SpaceshipSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/SpaceshipSSS.png", "assets/images/Level2/SpaceshipSSS.png");
			type.set ("assets/images/Level2/SpaceshipSSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/SqrtSS.png", "assets/images/Level2/SqrtSS.png");
			type.set ("assets/images/Level2/SqrtSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/Stage2.png", "assets/images/Level2/Stage2.png");
			type.set ("assets/images/Level2/Stage2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level2/SumSS.png", "assets/images/Level2/SumSS.png");
			type.set ("assets/images/Level2/SumSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/ClimbingSS.png", "assets/images/Level3/ClimbingSS.png");
			type.set ("assets/images/Level3/ClimbingSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/CoinSS.png", "assets/images/Level3/CoinSS.png");
			type.set ("assets/images/Level3/CoinSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/message3.png", "assets/images/Level3/message3.png");
			type.set ("assets/images/Level3/message3.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/Period3.png", "assets/images/Level3/Period3.png");
			type.set ("assets/images/Level3/Period3.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/PoisonSS.png", "assets/images/Level3/PoisonSS.png");
			type.set ("assets/images/Level3/PoisonSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/Stage31.png", "assets/images/Level3/Stage31.png");
			type.set ("assets/images/Level3/Stage31.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/Stage32.png", "assets/images/Level3/Stage32.png");
			type.set ("assets/images/Level3/Stage32.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level3/VialSS.png", "assets/images/Level3/VialSS.png");
			type.set ("assets/images/Level3/VialSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/ArrowsSS.png", "assets/images/Level4/ArrowsSS.png");
			type.set ("assets/images/Level4/ArrowsSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/DancingSS.png", "assets/images/Level4/DancingSS.png");
			type.set ("assets/images/Level4/DancingSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/LincolnSS.png", "assets/images/Level4/LincolnSS.png");
			type.set ("assets/images/Level4/LincolnSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/message4.png", "assets/images/Level4/message4.png");
			type.set ("assets/images/Level4/message4.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/NapoleonSS.png", "assets/images/Level4/NapoleonSS.png");
			type.set ("assets/images/Level4/NapoleonSS.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/Period4.png", "assets/images/Level4/Period4.png");
			type.set ("assets/images/Level4/Period4.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/Stage1.png", "assets/images/Level4/Stage1.png");
			type.set ("assets/images/Level4/Stage1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/Track.png", "assets/images/Level4/Track.png");
			type.set ("assets/images/Level4/Track.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Level4/XCheckmark.png", "assets/images/Level4/XCheckmark.png");
			type.set ("assets/images/Level4/XCheckmark.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/MainMenu/charmenu.png", "assets/images/MainMenu/charmenu.png");
			type.set ("assets/images/MainMenu/charmenu.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/MainMenu/insertcoin.png", "assets/images/MainMenu/insertcoin.png");
			type.set ("assets/images/MainMenu/insertcoin.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/MainMenu/menubg.png", "assets/images/MainMenu/menubg.png");
			type.set ("assets/images/MainMenu/menubg.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/MainMenu/playbutton.png", "assets/images/MainMenu/playbutton.png");
			type.set ("assets/images/MainMenu/playbutton.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/MainMenu/settingsbutt.png", "assets/images/MainMenu/settingsbutt.png");
			type.set ("assets/images/MainMenu/settingsbutt.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/MainMenu/title.png", "assets/images/MainMenu/title.png");
			type.set ("assets/images/MainMenu/title.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Win.png", "assets/images/Win.png");
			type.set ("assets/images/Win.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/music/develop.wav", "assets/music/develop.wav");
			type.set ("assets/music/develop.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/drowsy1.wav", "assets/music/drowsy1.wav");
			type.set ("assets/music/drowsy1.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/drowsy2.wav", "assets/music/drowsy2.wav");
			type.set ("assets/music/drowsy2.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/hyper-partial4.wav", "assets/music/hyper-partial4.wav");
			type.set ("assets/music/hyper-partial4.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/music-goes-here.txt", "assets/music/music-goes-here.txt");
			type.set ("assets/music/music-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/sounds/Coin.wav", "assets/sounds/Coin.wav");
			type.set ("assets/sounds/Coin.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Dead.wav", "assets/sounds/Dead.wav");
			type.set ("assets/sounds/Dead.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Dead2.wav", "assets/sounds/Dead2.wav");
			type.set ("assets/sounds/Dead2.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Explosion.wav", "assets/sounds/Explosion.wav");
			type.set ("assets/sounds/Explosion.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Hit_hurt.wav", "assets/sounds/Hit_hurt.wav");
			type.set ("assets/sounds/Hit_hurt.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Laser.wav", "assets/sounds/Laser.wav");
			type.set ("assets/sounds/Laser.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Punch1.wav", "assets/sounds/Punch1.wav");
			type.set ("assets/sounds/Punch1.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Punch2.wav", "assets/sounds/Punch2.wav");
			type.set ("assets/sounds/Punch2.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/sounds-go-here.txt", "assets/sounds/sounds-go-here.txt");
			type.set ("assets/sounds/sounds-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("flixel/sounds/beep.ogg", "flixel/sounds/beep.ogg");
			type.set ("flixel/sounds/beep.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("flixel/sounds/flixel.ogg", "flixel/sounds/flixel.ogg");
			type.set ("flixel/sounds/flixel.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("flixel/fonts/nokiafc22.ttf", "flixel/fonts/nokiafc22.ttf");
			type.set ("flixel/fonts/nokiafc22.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
			path.set ("flixel/fonts/monsterrat.ttf", "flixel/fonts/monsterrat.ttf");
			type.set ("flixel/fonts/monsterrat.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
			path.set ("flixel/images/ui/button.png", "flixel/images/ui/button.png");
			type.set ("flixel/images/ui/button.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("flixel/images/logo/default.png", "flixel/images/logo/default.png");
			type.set ("flixel/images/logo/default.png", Reflect.field (AssetType, "image".toUpperCase ()));
			
			
			initialized = true;
			
		} //!initialized
		
	} //initialize
	
	
} //AssetData
