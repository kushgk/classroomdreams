package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.group.*;

class SpaceState extends FlxState
{
	var player: PlayerShip;
	var enemies: FlxGroup;
	var playerBullets: FlxGroup;
	var enemyBullets: FlxGroup;
	var timer: Float;

	override public function create():Void
	{
		super.create();

		add(new FlxSprite(0, 0, "assets/images/Level2/Stage2.png"));

		playerBullets = new FlxGroup(80);
		enemyBullets = new FlxGroup(80);
		enemies = new FlxGroup(32);

		for (j in 1 ... 2) {
			for (i in 0 ... 8) {
				enemies.add(new Integral(32*i,32*j));
			}
		}
		player = new PlayerShip();
		add(player);
		add(playerBullets);
		add(enemyBullets);
		add(enemies);

		timer = 0;
		FlxG.sound.playMusic("assets/music/drowsy1.wav", 1, true);
	}

	override public function update(elapsed: Float){

		if (FlxG.sound.music == null) // don't restart the music if it's already playing
	 	{
    		FlxG.sound.playMusic("assets/music/drowsy1.wav", 1, true);
		}

		if(enemies.countLiving() == 0 || enemies.countLiving() == -1){
			FlxG.switchState(new ChemIntro());
		}

		timer --;
		if (FlxG.keys.anyPressed([SPACE]) && timer < 0)
		{
			playerBullets.add(new PlayerBullet(player.x+(player.width/2),player.y));
			timer = 20;
		}

		enemyShoot(enemies.getRandom());

		FlxG.overlap(enemies,playerBullets,killEnemy);
		FlxG.overlap(player,enemyBullets,killPlayer);
		super.update(elapsed);
	}

	private function killEnemy(enemy, bullet){
		bullet.destroy();
		//enemies.remove(enemy);
		//add(enemy);
		enemy.die();
	}

	private function killPlayer(play, bullet){
		if (FlxG.pixelPerfectOverlap(play, bullet)) {
			play.destroy();
			bullet.destroy();
			FlxG.switchState(new SpaceLose());

		}
	}

	private function enemyShoot(enem){
		var enemy: Integral = cast enem;
		if(Math.random() < (0.034+0.0034)/2){
			enemyBullets.add(new Enemybullet.EnemyBullet(enemy.x+(enemy.width/2),enemy.y+enemy.height));
		}
	}
}
