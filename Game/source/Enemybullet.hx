package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;

class EnemyBullet extends FlxSprite{
	public function new(x,y){
		super(x, y, "assets/images/Level2/Enemybullet.png");
		velocity.y = 180;
	}
}