package;

import flixel.FlxState;
import flixel.FlxG;

class DDRIntro extends FlxState
{
    override public function create():Void
    {
        super.create();
        add(new flixel.FlxSprite(0,0,"assets/images/Level4/Period4.png"));
        FlxG.sound.pause();
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);
        if(FlxG.mouse.pressed || FlxG.keys.anyPressed([SPACE])){
        	FlxG.switchState(new DDRState());
        }
    }
}