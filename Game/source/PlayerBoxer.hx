package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;

/**
* Class Declaration for the player boxer
*/
class PlayerBoxer extends FlxSprite
{

	public var book:Book;

	public function new()
	{
		super(130,100);

		loadGraphic("assets/images/Level1/MCSS.png",true, 64, 64);
		animation.add("RightPunch" , [6,7,8,9],8,false);
		animation.add("LeftPunch" , [38,39,40,41],8,false);
		animation.add("Hit" , [12,13],5,false);
		animation.add("Ded", [18,19,20,21,22,23], 5, false );
		animation.add("Idle" , [0,1,2],5,true);
		animation.add("LefDod", [24,25,26,27,28],8,false);
		animation.add("RigDod", [55,56,57,58,59],8, false);
		animation.play("Idle");

		animation.finishCallback = stateChange;

		health = 200;

	}





	override public function update(elapsed:Float):Void
	{
		if(health <= 0){
			FlxG.switchState(new BoxerLose());
		}
		if (FlxG.keys.anyPressed([X]) && animation.name != "RightPunch" && animation.name != "LeftPunch")
		{
			animation.play("RightPunch");
		}

		if (FlxG.keys.anyPressed([LEFT]))
		{
			animation.play("LefDod");
		}

		if (FlxG.keys.anyPressed([RIGHT]))
		{

		animation.play("RigDod");

		}

		if (FlxG.keys.anyPressed([Z]) && animation.name != "RightPunch" && animation.name != "LeftPunch")
		{

			animation.play("LeftPunch");

		}

		if (health <= 0)
		{

			FlxG.switchState(new BoxerLose());


		}


		super.update(elapsed);


	}



 	public function stateChange(anim: String)
 	{
	 	switch (anim){
	 		case "RightPunch":
	               animation.play("Idle");
				   if(!( book.animation.name=="leftPunch" || book.animation.name=="rightPunch"  ||  book.animation.name=="hit" )){
					   book.health -= 10;
					   book.animation.play("hit");
				   }

	        
	        case "LeftPunch":
				if(!( book.animation.name=="leftPunch" || book.animation.name=="rightPunch"  ||  book.animation.name=="hit" )){
					book.health -= 10;
					book.animation.play("hit");
				}
	        	animation.play("Idle");
	        
	        default:
	            animation.play("Idle");
 		}
	}
}
