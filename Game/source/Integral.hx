package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.group.FlxGroup;


class Integral extends FlxSprite {
	private var _shotClock:Float;
	private var _originalX:Int;
	private var stepCount: Int;
	private var dir: Int;
	private var moving: Bool;

	public function new(x, y)
	{
		super(x, y);
		loadGraphic("assets/images/Level2/IntegrateSS.png", true);


		this.animation.add("Default", [0], Math.floor(6 + FlxG.random.float() * 4));


		this.animation.play("Default");

		stepCount = -59;
		moving = true;
		dir = 1;
	}

	override public function update(elapsed: Float){
		if(moving){
			if(stepCount < 120){
				if(stepCount % 60 == 0){
					x += dir*32;
				}
				stepCount ++;
			} else{
				if(stepCount % 60 == 0){
					y += 32;
					stepCount = -59;
					dir *= -1;
				}
			}
		}

		if(y>(240-64)){
			FlxG.switchState(new MenuState());
		}

		super.update(elapsed);
	}

	public function die(){
		moving = false;
		loadGraphic("assets/images/Level2/PuffSS.png", true, 48, 48);
		animation.add("blow up", [0,1,2,3,4],8,false);
		animation.finishCallback = suicide;
		animation.play("blow up");
	}

	public function suicide(fummy:String){
		this.destroy();
	}

}
