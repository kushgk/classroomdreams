package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.group.*;
import flixel.util.*;

class DDRState extends FlxState{
	private var player: FlxSprite;
	private var lincoln: FlxSprite;
	private var napoleon: FlxSprite;
	private var beatCounter: Int;
	private var beats: Array<String>;
	private var dancers: FlxGroup;
	private var prompts: FlxGroup;
	private var outputs: FlxGroup;
	private var failure: Int;

	override public function create(){
		add(new FlxSprite(0,0,"assets/images/Level4/Stage1.png"));

		beatCounter = 0;
		failure = 0;

		dancers = new FlxGroup(3);
		prompts = new FlxGroup(999);
		outputs = new FlxGroup(999);

		player = new FlxSprite(160-32,120);
		player.loadGraphic("assets/images/Level4/DancingSS.png", true, 64, 64);
		player.animation.add("Down",[0,1], 6, true);
		player.animation.add("Left",[2,3], 6, true);
		player.animation.add("Right",[4,5], 6, true);
		player.animation.add("Up",[6,7],6, true);
		dancers.add(player);

		napoleon = new FlxSprite(96-32,88);
		napoleon.loadGraphic("assets/images/Level4/NapoleonSS.png", true, 64, 64);
		napoleon.animation.add("Down",[0,1], 6, true);
		napoleon.animation.add("Left",[2,3], 6, true);
		napoleon.animation.add("Right",[4,5], 6, true);
		napoleon.animation.add("Up",[6,7],6, true);
		dancers.add(napoleon);

		lincoln = new FlxSprite(224-32,88);
		lincoln.loadGraphic("assets/images/Level4/LincolnSS.png", true, 64, 64);
		lincoln.animation.add("Down",[0,1], 6, true);
		lincoln.animation.add("Left",[2,3], 6, true);
		lincoln.animation.add("Right",[4,5], 6, true);
		lincoln.animation.add("Up",[6,7],6, true);
		dancers.add(lincoln);

		add(new FlxSprite(0, 4, "assets/images/Level4/Track.png"));
		add(dancers);
		add(prompts);
		add(outputs);

		FlxG.sound.playMusic("assets/music/hyper-partial4.wav", 1, true);

		beats = ["d", "l", "r", "l", "r", "l", "r", "l", "d", "l", "r", "l", "r", "_", "_", "_", "d", "l", "r", "l", "r", "l", "r", "l", "d", "l", "r", "l", "r", "l", "r", "l", "u", "_", "_", "l", "_", "_", "r", "_", "_", "u", "_", "_", "d", "_", "_", "r", "l", "l", "l", "d", "d", "_", "_", "d", "l", "r", "l", "r", "l", "r", "l", "d", "l", "r", "l", "r", "_", "_", "_", "d", "l", "r", "l", "r", "l", "r", "l", "d", "l", "r", "l", "r", "l", "r", "l", "u", "_", "_", "l", "_", "_", "r", "_", "_", "u", "_", "_", "d", "_", "_", "r", "l", "l", "l", "d", "d", "_", "_", "d", "l", "r", "l", "r", "l", "r", "l", "d", "l", "r", "l", "r"];
	}

	override public function update(elapsed: Float){
		beatCounter ++;

		if(failure > 9){
			FlxG.switchState(new DDRLose());
		}

		if(beats.length == 0){
			FlxG.switchState(new MenuState());
		}

		prompts.forEach(function(obj){
			var member: FlxSprite = cast obj;
			member.x -= 58/9;
			if(member.x < -1*member.width){
				prompts.remove(member);
				failure ++;
			}

		});

		outputs.forEach(function(obj){
			var member: FlxSprite = cast obj;
			member.x -= 58/9;
			if(member.x < -1*member.width){
				prompts.remove(member);
			}

		});

		if(FlxG.keys.anyJustPressed([UP,DOWN,RIGHT,LEFT])){
			if(prompts.getFirstAlive() != null){
				var correct = false;
				var first: FlxSprite = cast prompts.getFirstAlive();
				prompts.forEach(function(obj){
					var member: FlxSprite = cast obj;
					if(member.x < first.x){
						first = member;
					}
				});
				switch (first.animation.frameIndex) {
					case 1:
						if(FlxG.keys.anyJustPressed([UP]) && Math.abs(first.x-4) < 20){
							correct = true;
							dancers.forEach(function(obj){
								var dancer: FlxSprite = cast obj;
								dancer.animation.play("Up");
							});
						} 
					case 0:
						if(FlxG.keys.anyJustPressed([LEFT]) && Math.abs(first.x-4) < 20){
							correct = true;
							dancers.forEach(function(obj){
								var dancer: FlxSprite = cast obj;
								dancer.animation.play("Left");
							});
						} 
					case 2:
						if(FlxG.keys.anyJustPressed([RIGHT]) && Math.abs(first.x-4) < 20){
							correct = true;
							dancers.forEach(function(obj){
								var dancer: FlxSprite = cast obj;
								dancer.animation.play("Right");
							});
						} 
					case 3:
						if(FlxG.keys.anyJustPressed([DOWN]) && Math.abs(first.x-4) < 20){
							correct = true;
							dancers.forEach(function(obj){
								var dancer: FlxSprite = cast obj;
								dancer.animation.play("Down");
							});
						} 
				}
				if(correct){
					first.loadGraphic("assets/images/Level4/XCheckmark.png",true,40,40);
					first.animation.frameIndex = 1;
					prompts.remove(first);
					outputs.add(first);

				}else{
					first.loadGraphic("assets/images/Level4/XCheckmark.png",true,40,40);
					first.animation.frameIndex = 0;
					prompts.remove(first);
					outputs.add(first);
					failure ++;
				}
			} 
		}

		if(beatCounter % 24 == 0){
			var arrow = new FlxSprite(320,8);
			arrow.loadGraphic("assets/images/Level4/ArrowsSS.png", true, 40,40 );
			switch(beats.shift()){
				case "u":
					arrow.animation.frameIndex = 1;
					prompts.add(arrow);
				case "l":
					arrow.animation.frameIndex = 0;
					prompts.add(arrow);
				case "r":
					arrow.animation.frameIndex = 2;
					prompts.add(arrow);
				case "d":
					arrow.animation.frameIndex = 3;
					prompts.add(arrow);
			}
		}
		super.update(elapsed);
	}
}