package;

import flixel.FlxState;
import flixel.FlxG;

class ChemLose extends FlxState
{
    override public function create():Void
    {
        super.create();
        add(new flixel.FlxSprite(0,0,"assets/images/GameOVer.png"));
        add(new flixel.FlxSprite(10,125,"assets/images/Level3/message3.png"));
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);
        if(FlxG.mouse.pressed){
        	FlxG.switchState(new MenuState());
        }
    }
}