package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;

/**
* Class Declaration for the player boxer
*/
class PlayerShip extends FlxSprite
{



	public function new(X:Float = 0, Y:Float = 0, Width:Float = 0, Height:Float = 0)
	{


		super(FlxG.width / 2 - 12, FlxG.height - 64, "assets/images/Level2/SpaceshipSSS.png");



	}
	override public function update(elapsed:Float):Void
	{

		velocity.x = 0;


		if (FlxG.keys.anyPressed([LEFT, A]))
		{
			velocity.x -= 150;
		}

		if (FlxG.keys.anyPressed([RIGHT, D]))
		{
			velocity.x += 150;
		}


		super.update(elapsed);




		if (x > FlxG.width - width - 4)
		{
			x = FlxG.width - width - 4;
		}


		if (x < 4)
		{
			x = 4;
		}
		super.update(elapsed);
			}
		}
