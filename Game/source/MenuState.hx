package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;

class MenuState extends FlxState
{
	override public function create():Void
	{
		super.create();
		var bg = new FlxSprite(0,0, 'assets/images/MainMenu/menubg(1).png');
		add(bg);

		var title = new FlxSprite(80,25,"assets/images/MainMenu/title.png");
		add(title);

		var playButt = new FlxButton(130, 138, "", play);
		playButt.loadGraphic("assets/images/MainMenu/playbutton(1).png", true, 66, 22);
		add(playButt);
		var setButt = new FlxButton(90, 167, "", settings);
		setButt.loadGraphic("assets/images/MainMenu/settingsbutt(1).png", true, 156, 22);
		add(setButt);

		var insertCoin = new FlxSprite(100,222,"assets/images/MainMenu/insertcoin.png");
		add(insertCoin);

		FlxG.sound.playMusic("assets/music/hyper-partial4.wav", 1, true);
	}

	override public function update(elapsed:Float):Void
	{
		if (FlxG.sound.music == null) // don't restart the music if it's already playing
	 	{
    		FlxG.sound.playMusic("assets/music/hyper-partial4.wav", 1, true);
		}
		super.update(elapsed);
	}

	private function play(){
		FlxG.switchState(new BookIntro());
	}

	private function settings(){
		FlxG.fullscreen = !FlxG.fullscreen;
	}
}
