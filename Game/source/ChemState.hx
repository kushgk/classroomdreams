package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.group.*;

class ChemState extends FlxState{

	var bg1: FlxSprite;
	var bg2: FlxSprite;
	var floor: Int;
	var player: FlxSprite;
	var obstacles: FlxGroup;
	var coin: FlxSprite;

	override public function create():Void
	{
		super.create();
    	bg1 = new FlxSprite(0,0,"assets/images/Level3/Stage31.png");
    	bg2 = new FlxSprite(0,-240,"assets/images/Level3/Stage31.png");
    	add(bg1);
    	add(bg2);
    	floor = 0;


    	player = new FlxSprite (160,240-64);
    	player.loadGraphic("assets/images/Level3/ClimbingSS.png", true, 64, 64);
    	player.animation.add("upClimb",[0,1,2,3,2,1], 8, true);
    	player.animation.add("downClimb",[3,2,1,0,1,2], 8, true);
    	player.animation.add("rightClimb",[4,5,6,5],8, true);
    	player.animation.add("leftClimb",[6,5,4,5],8, true);
    	add(player);

    	obstacles = new FlxGroup(999);
    	add(obstacles);

    	coin = new FlxSprite((320-32)/2, -64);
    	coin.loadGraphic("assets/images/Level3/CoinSS.png", true, 32, 32);
    	coin.animation.add("spin",[0,1,2,3,2,1], 8, true);

    	FlxG.sound.playMusic("assets/music/drowsy2.wav", 1, true);
	}

	override public function update(elapsed: Float){

		if (FlxG.sound.music == null) // don't restart the music if it's already playing
	 	{
    		FlxG.sound.playMusic("assets/music/drowsy2.wav", 1, true);
		}

		FlxG.overlap(player, obstacles, lose);

		if(bg1.y > 240){
			bg1.y = -240;
			floor ++;
		}

		if(bg2.y > 240){
			bg2.y = -240;
			floor ++;
		}

		if(bg1.y < -240){
			bg1.y = 240;
			floor --;
		}

		if(bg2.y < -240){
			bg2.y = 240;
			floor --;
		}

		if (FlxG.keys.anyPressed([UP, W]))
		{
			bg1.velocity.y = 50;
			bg2.velocity.y = 50;
			coin.velocity.y = 50;
			obstacles.forEach(function(member){
				var obj: FlxSprite = cast member;
				obj.y += 0.6;
			});
			player.animation.play("upClimb");
		}else if (FlxG.keys.anyPressed([DOWN, S]))
		{
			bg1.velocity.y = -50;
			bg2.velocity.y = -50;
			coin.velocity.y = -50;
			obstacles.forEach(function(member){
				var obj: FlxSprite = cast member;
				obj.y -= 0.6;
			});
			player.animation.play("upClimb");
		} else {
			bg1.velocity.y = 0;
			bg2.velocity.y = 0;
			coin.velocity.y = 0;
			if (FlxG.keys.anyPressed([LEFT,A]) && player.x > 83)
			{
				player.velocity.x = -50;
				player.animation.play("leftClimb");
			}else if (FlxG.keys.anyPressed([RIGHT,D]) && player.x < (235-player.width))
			{
				player.velocity.x = 50;
				player.animation.play("rightClimb");
			}else{
				player.velocity.x = 0;
				player.animation.pause();
			}
		}

		if(!(FlxG.keys.anyPressed([LEFT,A]) || FlxG.keys.anyPressed([RIGHT,D]))){
			player.velocity.x = 0;
		}

		if(floor == 4){
			bg2.loadGraphic("assets/images/Level3/Stage32.png");
			add(coin);
			coin.animation.play("spin");
			floor = 10;
		}


		if(floor ==11){
			FlxG.switchState(new DDRIntro());
		}

		if(Math.random() < 0.009){
			var poison = new FlxSprite(Math.random()*(223-83) + 83, 0);
			poison.loadGraphic("assets/images/Level3/VialSS.png", true, 23, 24);
			poison.animation.add("fall",[0,1,2,3,4,5,6,7],10,true);
			poison.animation.add("die", [8,9,10,11], 5, false);
			obstacles.add(poison);
			poison.velocity.y = 30;
			poison.acceleration.y = 10;
			poison.animation.play("fall");
		}

		super.update(elapsed);
	}

	private function lose(player, obstacle){
		if(FlxG.pixelPerfectOverlap(player, obstacle)){
			obstacle.animation.play("die");
			obstacle.animation.finishCallback = nextLevel;
		}
	}

	private function nextLevel(junk: String){
		FlxG.switchState(new ChemLose());
	}
}
