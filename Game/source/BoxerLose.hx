package;

import flixel.FlxState;
import flixel.FlxG;

class BoxerLose extends FlxState
{
    override public function create():Void
    {
        super.create();
        	add(new flixel.FlxSprite(0,0,"assets/images/GameOVer.png"));
        	add(new flixel.FlxSprite(55,125,"assets/images/Level1/message1.png"));
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);
        if(FlxG.mouse.pressed){
        	FlxG.switchState(new MenuState());
        }
    }
}