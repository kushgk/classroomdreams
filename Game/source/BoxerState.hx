package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.ui.FlxBar;
import flixel.math.FlxMath;
import flixel.util.FlxColor;

class BoxerState extends FlxState
{
	override public function create():Void
	{
		super.create();
		add(new FlxSprite(0, 0, "assets/images/Level1/Stage.png"));
		var player = new PlayerBoxer();
        var book = new Book(player);
		add(book);
		add(player);
        var Ebar = new FlxBar(160-50, 5, LEFT_TO_RIGHT, 100, 10, book, "health", 0, 200, true);
       Ebar.createGradientFilledBar([FlxColor.GREEN,  FlxColor.YELLOW , FlxColor.RED] ,5, 180  );
       add(Ebar);
        var Pbar = new FlxBar(160-50, 225, LEFT_TO_RIGHT, 100, 10, player, "health", 0, 200, true);
         Pbar.createGradientFilledBar([FlxColor.GREEN,  FlxColor.YELLOW , FlxColor.RED] ,5, 180  );
         add(Pbar);
		add(new FlxSprite(0, 0, "assets/images/Level1/StageOverlay.png"));
		FlxG.sound.playMusic("assets/music/drowsy2.wav", 1, true);
	}

	override public function update(elapsed:Float):Void
	{
		if (FlxG.sound.music == null) // don't restart the music if it's already playing
	 	{
    		FlxG.sound.playMusic("assets/music/drowssy2.wav", 1, true);
		}
		super.update(elapsed);
	}

}
