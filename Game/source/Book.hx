package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;

class Book extends FlxSprite {

    public var player:PlayerBoxer;

    public function new(play:PlayerBoxer) {
        super(120, 20);
        loadGraphic("assets/images/Level1/BookSheet.png", true, 100, 112);
        animation.add("idle", [0,1], 5, false);
        animation.add("leftPunch",[5,6,7,8,9], 5, false);
        animation.add("rightPunch",[10,11,12,13,14], 5, false);
        animation.add("knock",[15,16,17,18], 3, false);
        animation.add("hit", [2], 5, false);
        animation.finishCallback = stateChange;

        health = 200;
        player = play;
        player.book = this;
        animation.play("idle");
    }

    public function stateChange(anim: String) {
        switch (anim) {
            case "leftPunch":
                animation.play("idle");
                if(player.animation.name != "RigDod"){
                    player.health -= 34;
                }
            case "rightPunch":
                animation.play("idle");
                if(player.animation.name != "LefDod"){
                    player.health -= 34;
                }
            case "idle":
                if(Math.round(Math.random())==0)
                {
                
                    if(Math.round(Math.random())==0) {
                        animation.play("leftPunch");
                    } 
                    else {
                        animation.play("rightPunch");
                    }
                }
                else
                {
                    animation.play("idle");
                }
            case "knock":
                FlxG.switchState(new SpaceIntro());
            case "hit":
                animation.play("idle");
            default:
                animation.play("idle");
        }
    }

    override public function update(elapsed:Float){
        if(health <= 0){
            animation.play("knock");
        }

        super.update(elapsed);
    }
}
